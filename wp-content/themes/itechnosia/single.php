<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */

get_header(); ?>

<?php
// Start the loop.
while ( have_posts() ) : the_post();

	if (  is_singular( 'works' ) ) {
		// Include the single work content template.
		get_template_part( 'template-parts/work', 'single' );
	} else {
		// Include the single post content template.
		get_template_part( 'template-parts/content', 'single' );
	}


	/*
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() and  is_singular( 'post' ) ) {
		?>
		<section class="page-section pg-comments">
			<div class="container-fluid">
				<?php
				comments_template();
				?>				
			</div>
		</section>
		<?php
	}
	*/

	if ( is_singular( 'attachment' ) ) {
		// Parent post navigation.
		the_post_navigation( array(
			'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'itechnosia' ),
		) );

	} elseif ( is_singular( 'post' )  ) {
		// Previous/next post navigation.

		the_post_navigation( array(
			'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'itechnosia' ) . '</span> ' .
				'<span class="screen-reader-text">' . __( 'Next post:', 'itechnosia' ) . '</span> ' .
				'<span class="post-title">%title</span>',
			'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'itechnosia' ) . '</span> ' .
				'<span class="screen-reader-text">' . __( 'Previous post:', 'itechnosia' ) . '</span> ' .
				'<span class="post-title">%title</span>',
		) );

	}  elseif (  is_singular( 'works' ) ) {
		// Previous/next post navigation.

		$next_post = get_next_post();
	    $previous_post = get_previous_post();
	    the_post_navigation( array(
	        'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next work', 'itechnosia' ) . '</span> ' .
	            '<span class="screen-reader-text">' . __( 'Next work:', 'itechnosia' ) . '</span> ' .
	            '<span class="post-title">%title</span>' . get_the_post_thumbnail($next_post->ID,'thumbnail'),
	        'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous work', 'itechnosia' ) . '</span> ' .
	            '<span class="screen-reader-text">' . __( 'Previous work:', 'itechnosia' ) . '</span> ' .
	            '<span class="post-title">%title</span>' . get_the_post_thumbnail($previous_post->ID,'thumbnail'),
	    ) );


	}

	// End of the loop.
endwhile;
?>



<?php get_template_part( 'template-parts/foot', 'gradient' ); ?>

<?php get_footer(); ?>
