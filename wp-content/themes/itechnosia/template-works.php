<?php
/**
 * Template Name: Works Template
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */
?>

<?php get_header(); ?>

<?php get_template_part( 'template-parts/subpage-banner', 'page' ); ?>

<section class="page-section">
	<div class="container-fluid">
		<p class="page-section-subtitle text-center">Experience a level of our quality in delivering and customization works.</p>

		<div class="portfolio portfolio-home">
			<div class="portfolio-navs text-center text-lg-right">
				<div class="btn-group" role="group">
					<button class="btn btn-md" data-group="website">Website</button>
					<button class="btn btn-md" data-group="branding">Branding</button>
					<button class="btn btn-md" data-group="application">Application</button>
					<button class="btn btn-md" data-group="mobile">Mobile</button>
				</div>
			</div>
			<div class="portfolio-list row">
                  
				<?php

					$the_query = new WP_Query( 
										array(
											'posts_per_page'	=> 9,
											'post_type'			=>'works',
											'paged' 			=> get_query_var('paged') ? get_query_var('paged') : 1) 
										); 
					?>

					<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

					<div class="portfolio-item card col-sm-6 col-md-4" data-groups='["website", "branding"]' data-title="<?php echo get_the_title();?>">
						
						<a href="<?php the_permalink(); ?>" class="portfolio-link">			
							<div class="portfolio-inner">
								<div class="portfolio-image">			
									<img class="card-img-top" src="<?php echo get_the_post_thumbnail_url();?>" alt="<?php echo get_the_title();?>" title="<?php echo get_the_title();?>">
								</div>		
								<div class="card-body">
									<h5 class="card-title"><?php echo get_the_title(); ?></h5>
									<div class="card-text"><?php the_content(); ?></div>
									<span class="u-label u-label--sm u-label--danger" href="#">Website</span>
									<span class="u-label u-label--sm u-label--success" href="#">Branding</span>
								</div>
							</div>
						</a>

                    </div>     

					<?php
					endwhile;

				?>          


			</div>

			<div class="pagination">
				<?php				

					$big = 999999999; // need an unlikely integer
					echo paginate_links( 
							array(
								'base' 		=> str_replace( $big, '%#%', get_pagenum_link( $big ) ),
								'format' 	=> '?paged=%#%',
								'current' 	=> max( 1, get_query_var('paged') ),
								'total' 	=> $the_query->max_num_pages
					) );

					wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</section>


<?php get_template_part( 'template-parts/foot', 'gradient' ); ?>	

<?php get_footer(); ?>