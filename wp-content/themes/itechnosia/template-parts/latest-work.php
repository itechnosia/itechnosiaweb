<?php
/**
 * The template part for displaying latest work
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */

?>
<section class="page-section ps-latestproject pg-full-width">
<div class="container-fluid">
      <h2 class="page-section-title  text-center">Our <span>latest</span> works</h2>
      <p class="page-section-subtitle text-center">Experience a level of our quality in both design &amp; customization works.</p>

    

        <?php
          $queryObject = new WP_Query( 'post_type=works&posts_per_page=6' );
          // The Loop!
          if ($queryObject->have_posts()) {
              ?>
              <div class="work-carousel">
              <div class="work-carousel-wrap">
              <?php
              while ($queryObject->have_posts()) {
                  $queryObject->the_post();
                  ?>


          
                  <div class="work-carousel-item">
                    <a href="<?php the_permalink(); ?>" class="work-carousel-link" alt="<?php the_title(); ?>">
                      <div class="work-carousel-inner">
                        <div class="work-carousel-image">
                          <img src="<?php echo get_the_post_thumbnail_url();?>" class="img-fluid" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                        </div>
                        <div class="work-carousel-caption">
                          <h4><?php the_title(); ?></h4>
                          <span>Website</span>
                        </div>                  
                      </div>
                    </a>
                  </div>

              <?php
              }
              ?>              
                </div>
              </div>
              <?php
          }
          ?>


    <div class="text-center">
      <a href="<?php echo esc_url( home_url( '/our-works/' ) ); ?>" class="btn btn-warning btn-icon btn-wide btn-pill transition-splash-hover"><i class="icon-chevron-right"></i>More Works</a>
    </div>

</div>
</section>    