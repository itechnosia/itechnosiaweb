<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */
?>

 <section class="page-section banner-subpage  is-has-wave">
	<div class="banner-subpage-image">	
		<div class="banner-subpage-inner">				
			<?php
				$featured_img_url = get_the_post_thumbnail_url(); 
			?>
			<div class="dzsparallaxer auto-init out-of-bootstrap" data-options='{ direction: "normal"}'>
			    <div class="divimage dzsparallaxer--target" style="width: 100%; height: 150%; background-image: url('<?php echo $featured_img_url;?>')">
			    </div>
			</div>
			

			<div class="container-fluid text-center">
				<?php the_title( '<h1 class="banner-subpage-title text-white font-weight-semi-bold mb-0">', '</h1>' ); ?>
			</div>

				<figure class="bg-wave bg-pos-bottom-wave">
					<img class="js-svg-injector" src="<?php bloginfo('template_directory');?>/img/bg/wave-bottom.svg" alt="wave background">
				</figure>


		</div>
	</div>
</section>