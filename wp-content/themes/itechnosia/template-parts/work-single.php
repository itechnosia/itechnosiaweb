<?php
/**
 * The template part for displaying single work
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */
?>

 <section class="page-section page-columns"> 
    <div class="container">
    	<?php
    		$post_id =  get_the_id();
    	?>
        <div class="row">
            <div class="col-lg-7 page-column page-column-left">
                <div class="detail-thumb">
                	
                	<?php if (has_post_thumbnail()) : ?>
                    <div class="detail-thumb-item pb-4">
                      <div class="image-frame img-fluid">
                      	<?php
                      		echo get_the_post_thumbnail( $post_id, 'full', array( 'class' => 'rounded img-fluid' ) );
                      	?>
                      </div>
                    </div>
                	<?php endif;?>
                	
                </div>
            </div>
            <div class="col-lg-5  page-column page-column-right">
                <div class="sticky-top">

                  <div class="sidepanel-block">  
                    <div class="inner">                          

                      <?php the_title( '<h1 class="page-title h2 text-primary font-weight-semi-bold">', '</h1>' ); ?>
                      
                      <div class="wysiwyg">
                          <?php
                          	the_content();
                          ?>
                      </div>

                    </div>
                  </div>

                  <div class="sidepanel-block"> 
                    <div class="inner">      
                      <dl class="detail-list">
                          <dt class="detail-list-label h6">Client</dt>
                          <dd class="detail-list-value text-muted small">Polynates</dd>

                          <dt class="detail-list-label h6">Projects</dt>
                          <dd class="detail-list-value text-muted small">
                            <span class="u-label u-label--sm u-label--primary mb-2">Application</span>
                            <span class="u-label u-label--sm u-label--success mb-2">Re-Branding</span>
                            <span class="u-label u-label--sm u-label--warning mb-2">Website</span>
                          </dd>

                          <dt class="detail-list-label h6">Technology</dt>
                          <dd class="detail-list-value text-muted small">Wordpress, Adobe Photoshop, Adobe Ilustrator</dd>

                          <dt class="detail-list-label h6">Go to</dt>
                          <dd class="detail-list-value text-muted small">http://www.polynates.com</dd>
                      </dl>
                    </div>
                  </div>

                  <div class="sidepanel-block">   
                    <div class="inner">    
                      <h4 class="sidepanel-block-label h6">Share</h4>
                      <div class="sharethis-inline-share-buttons"></div>            
                    </div>
                  </div>
                  
                </div>
            </div>
        </div>
    </div>
</section>