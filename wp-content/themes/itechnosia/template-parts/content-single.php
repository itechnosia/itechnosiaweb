<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */
?>
<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */


?>
<section id="post-<?php the_ID(); ?>" class="page-section page-columns"> 
    <div class="container">


        <div class="row">
            <div class="col-lg-8 page-column page-column-left">
              
              <div class="post-info-bar">
                <small class="text-muted">
                  <i class="icon-calendar"></i>
                  <?php echo get_the_date();?>
                </small> 
                <small class="text-muted">
                  <i class="icon-comments"></i>
                  <?php 
                    $comNum = get_comments_number();
                    $comLabel = "";
                    if($comNum == 0):
                      $comLabel = " comment";
                    else: 
                      $comLabel = " comments";
                    endif; 

                    echo $comNum . $comLabel;
                  ?>
                </small>
              </div>  

              <?php the_title( '<h1 class="page-title h1 font-weight-semi-bold">', '</h1>' ); ?>
              
              <?php if ( has_post_thumbnail() ) : ?>
                  <figure class="post-image">
                  <?php
                  the_post_thumbnail();
                  ?>                    
                  </figure>

                <?php endif;?>

               <div class="wysiwyg">
                  <?php the_content();?>
               
               </div>

              <?php
               if ( comments_open() || get_comments_number() and  is_singular( 'post' ) ) {
                  ?>
                  <section class="page-section pg-comments">
                    <div class="container-fluid">
                      <?php
                      comments_template();
                      ?>        
                    </div>
                  </section>
                  <?php
                }
              ?>

            </div>
            <div class="col-lg-4 page-column page-column-right">
                <div class="sticky-top">

                  <?php get_sidebar(); ?>


                  <?php
            $content = get_post_field('post_content', $recent["ID"]);
            $content = strip_tags($content);
            echo substr($content, 0, 10);
                  ?>

                <div class="sidepanel-block">   
                   <div class="inner">                           
                      <h4 class="sidepanel-block-label h6">CATEGORIES</h4>
                      <ul class="list-unstyled blog-category-list">
                        <li>
                            <a href="#" title=""><span>Apartments</span></a>
                            <span>7</span>
                        </li>
                        <li>
                            <a href="#" title=""><span>Business</span></a>
                            <span>15</span>
                        </li>
                        <li>
                            <a href="#" title=""><span>Construction</span></a>
                            <span>4</span>
                        </li>
                        <li>
                            <a href="#" title=""><span>Location</span></a>
                            <span>1</span>
                        </li>
                    </ul>
                    </div>
                  </div>

                  <div class="sidepanel-block">  
                   <div class="inner">         
                    <h4 class="sidepanel-block-label h6">POPULAR POSTS</h4>  
                    <ul class="list-unstyled popular-post-list">
                      <li class="media">

                        <svg class="bd-placeholder-img mr-3" width="64" height="64" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 64x64"><title>Placeholder</title><rect fill="#868e96" width="100%" height="100%"></rect><text fill="#dee2e6" dy=".3em" x="50%" y="50%">64x64</text></svg>

                        <div class="media-body">
                          <h5 class="mt-0 mb-1">List-based media object</h5>
                          <span><i class="icon-calendar"></i>April 25, 2018</span>
                        </div>
                      </li>
                      <li class="media my-4">

                        <svg class="bd-placeholder-img mr-3" width="64" height="64" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 64x64"><title>Placeholder</title><rect fill="#868e96" width="100%" height="100%"></rect><text fill="#dee2e6" dy=".3em" x="50%" y="50%">64x64</text></svg>

                        <div class="media-body">
                          <h5 class="mt-0 mb-1">List-based media object</h5>
                          <span><i class="icon-calendar"></i>April 25, 2018</span>
                        </div>
                      </li>
                      <li class="media">
                        
                        <svg class="bd-placeholder-img mr-3" width="64" height="64" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 64x64"><title>Placeholder</title><rect fill="#868e96" width="100%" height="100%"></rect><text fill="#dee2e6" dy=".3em" x="50%" y="50%">64x64</text></svg>

                        <div class="media-body">
                          <h5 class="mt-0 mb-1">List-based media object</h5>
                          <span><i class="icon-calendar"></i>April 25, 2018</span>
                        </div>
                      </li>
                    </ul> 
                  </div>

                  </div>


                  <div class="sidepanel-block">  
                   <div class="inner">             
                    <h4 class="sidepanel-block-label h6">POPULAR TAGS</h4>
                    <span class="u-label u-label--sm u-label--primary mb-2">Application</span> <span class="u-label u-label--sm u-label--primary mb-2">Website</span> <span class="u-label u-label--sm u-label--primary mb-2">Design</span>
                  </div>
                  </div>


                </div>
            </div>
        </div>

      



    </div>
</section>