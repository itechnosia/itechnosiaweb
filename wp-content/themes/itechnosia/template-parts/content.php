<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */


?>

<div class="blog-item card">
	<a href="<?php echo get_permalink();?>">
	  <img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt="Card image cap">
	</a>
	<div class="card-body">
	  <div class="blog-info">
	    <div class="card-text">

	    	<small class="text-muted">
	    		<i class="icon-calendar"></i>
	    		<?php echo get_the_date();?>
	    	</small> 
	    	<small class="text-muted">
	    		<i class="icon-comments"></i>
	    		<?php 
	    			$comNum = get_comments_number();
	    			$comLabel = "";
	    			if($comNum == 0):
	    				$comLabel = " comment";
	    			else: 
	    				$comLabel = " comments";
	    			endif; 

	    			echo $comNum . $comLabel;
	    		?>
	    	</small>
	    </div>                                
	  </div>

	  <?php the_title( sprintf( '<h5 class="card-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>' ); ?>
	  
	  <div class="card-text">
	  	<?php 
	  		
	  		$trimexcerpt = get_the_excerpt();	
			$shortexcerpt = wp_trim_words( $trimexcerpt, $num_words = 15, $more = ' . . .' ); 
	  		echo $shortexcerpt; 

	  	?></div>
	</div>
</div>