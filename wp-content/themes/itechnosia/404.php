<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */

get_header(); ?>
<section class="page-section pg-404 error-404 not-found">
	<div class="container-fluid">	
			<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'itechnosia' ); ?></h1>

		<div class="page-content">
			<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'itechnosia' ); ?></p>

			<?php get_search_form(); ?>
		</div><!-- .page-content -->
	</div>
</section><!-- .error-404 -->
<?php get_footer(); ?>
