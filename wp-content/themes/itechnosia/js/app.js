(function () {
    var root = this;
    var Core = function (obj) {
        if (obj instanceof Core) {
            return obj;
        }
        if (!(this instanceof Core)) {
            return new Core(obj);
        }
    };
    root.Core = Core;
}).call(this);
Core.SetCache = function () {
    Core.Cache = {
        $window: $(window),
        $document: $(document),
        $body: $('body')
    };
};
Core.Components = function () {
    Core.Components.init();
};
Core.Config = Core.Config || {};
Core.IsMobile = function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (width <= Core.Config.MobileWidth) {
            return true;
        }
    }
    return false;
};
Core.IsTablet = function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (width <= Core.Config.TabletWidth) {
            return true;
        }
    }
    return false;
};
Core.IsAppleTouch = function () {
    var ua = navigator.userAgent.toUpperCase();
    var ipad = ua.indexOf('IPAD') > -1, ipod = ua.indexOf('IPOD') > -1, iphone = ua.indexOf('IPHONE') > -1;
    return ipad || ipod || iphone;
};
Core.CustomSelect = function (el, obj) {
    $(el).selectpicker(obj);
    $(el).selectpicker('setStyle', 'btn dropdown-toggle', 'remove');
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $(el).selectpicker('mobile');
    }
};
Core.SumoSelectCustom = function (el, obj) {
    $(el).SumoSelect(obj);
};
Core.RemoveJSONMessage = function () {
    var $messageBox = $('.json-message');
    $messageBox.on('click', function () {
        this.remove();
    });
    setTimeout(function () {
        $messageBox.slideUp(500, function () {
            this.remove();
        });
    }, 10000);
};
Core.RenderTemplate = function (templateName, data, $placeholder) {
    var source = $(templateName).html();
    var template = Handlebars.compile(source);
    $placeholder.html(template(data));
};
Core.GetParameterByName = function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};
Core.UpdateUrlParameter = function (uri, key, value) {
    // remove the hash part before operating on the uri
    var i = uri.indexOf('#');
    var hash = i === -1 ? '' : uri.substr(i);
    uri = i === -1 ? uri : uri.substr(0, i);
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        uri = uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        uri = uri + separator + key + "=" + value;
    }
    return uri + hash; // finally append the hash as well
};
String.prototype.strim = function (needle) {
    var org = this;
    var modified = "";
    var first_pos = 0;
    var last_pos = org.length - 1;
    //find first non needle char position
    for (var i = 0; i < org.length; i++) {
        if (org.charAt(i) !== needle) {
            first_pos = (i == 0 ? 0 : i);
            break;
        }
    }
    // find first non needle char position
    for (var a = org.length - 1; a > 0; a--) {
        if (org.charAt(a) !== needle) {
            last_pos = (a == org.length ? org.length : i + 1);
            break;
        }
    }
    return org.substring(first_pos, last_pos);
};
Core.Loading = (function () {
    var initShow = function () {
        Core.Cache.$body.addClass("showLoading");
    };
    var initHide = function () {
        Core.Cache.$body.removeClass("showLoading");
    };
    return {
        Show: initShow,
        Hide: initHide
    };
})();
Core.ShowPageLoading = function (message) {
    var msg = message || false;
    el = $('#loading-overlay');
    if (message) {
        if (el.length === 0) {
            if (msg != "")
                $('body').prepend('<div id="loading-overlay" class="loading-overlay has-message" style="display:block;"><span class="lv1"><span class="lv2">' + msg + '</span></span></div>');
            else
                $('body').prepend('<div id="loading-overlay" class="loading-overlay no-message" style="display:block;"></div>');
        }
        else {
            if (msg != "")
                el
                    .addClass('has-message')
                    .removeClass('no-message')
                    .append('<span class="lv1"><span class="lv2">' + msg + '</span></span>')
                    .show();
            else
                el
                    .addClass('no-message')
                    .removeClass('has-message')
                    .html("")
                    .show()
                    .end();
        }
    }
    else {
        Core.Cache.$body.addClass('showLoading');
    }
};
Core.HidePageLoading = function () {
    Core.Cache.$loadingOverlay.hide();
    $('#loading-overlay')
        .hide()
        .html("")
        .removeClass('no-message has-message');
    Core.Cache.$body.removeClass('showLoading');
};
/**
 * Mobile safari doesn't lose focus when tapping outside an input/textarea, so needs s workaround.
 * REF: https://stackoverflow.com/questions/30049222/iphone-keyboard-not-hiding-when-tapping-the-screen#answer-34944177
 */
Core.mobileSafariUnfocusWorkaround = function () {
    var cacheInput = null;
    var timer = null;
    if (!Core.IsAppleTouch()) {
        return false;
    }
    Core.Cache.$document.on('focus', 'input', function (e) {
        if ($(e.target).closest(".selectize-input").length)
            return;
        cacheInput = e.target;
    });
    Core.Cache.$document.on('focus', 'textarea', function (e) {
        if ($(e.target).closest(".selectize-input").length)
            return;
        cacheInput = e.target;
    });
    Core.Cache.$document.on('touchend', function (e) {
        if (e.target.tagName !== 'INPUT' && e.target.tagName !== 'TEXTAREA') {
            if (cacheInput !== null) {
                timer = setTimeout(function () {
                    cacheInput.blur();
                    clearTimeout(timer);
                }, 300);
            }
        }
    });
};
Core.SetCache();
Core.Cache.$document.ready(function () {
    Core.mobileSafariUnfocusWorkaround();
    //Core.Engine.init();
    Core.Components({});
});
Core.Components.SetCache = function () {
    this.Cache = {
        button: document.querySelector('.btn'),
        bannerCarousel: document.querySelector('.banner-carousel'),
        navbarToggle: document.querySelector('.navbar-toggler'),
        shuffle: document.querySelector('.portfolio-home'),
        testimonials: document.querySelector('.testimonials'),
        textTyping: document.querySelector('.text-typing'),
        workCarousel: document.querySelector('.work-carousel'),
    };
};
Core.Components.init = function () {
    this.SetCache();
    /**
     * @see core/button.js
    */
    if (this.Cache.button) {
        Core.Button.init();
    }
    /**
     * @see core/bannerCarousel.js
    */
    if (this.Cache.bannerCarousel) {
        Core.BannerCarousel.init();
    }
    /**
     * @see core/navbarToggle.js
     */
    if (this.Cache.navbarToggle) {
        Core.NavbarToggle.init();
    }
    /**
     * @see core/shuffle.js
    */
    if (this.Cache.shuffle) {
        Core.Shuffle.init();
    }
    /**
     * @see core/testimonials.js
    */
    if (this.Cache.testimonials) {
        Core.Testimonials.init();
    }
    /**
     * @see core/textTyping.js
    */
    if (this.Cache.textTyping) {
        Core.TextTyping.init();
    }
    /**
     * @see core/workCarousel.js
    */
    if (this.Cache.workCarousel) {
        Core.WorkCarousel.init();
    }
};
Core.BannerCarousel = {
    init: function () {
        Core.BannerCarousel.Cache = {
            $BannerCarousel: $('.banner-carousel-inner')
        };
        this.initialize();
    },
    initialize: function () {
        Core.BannerCarousel.Cache.$BannerCarousel.each(function () {
            var $carousel = $(this);
            var slidesToShow = Core.BannerCarousel.getCustomSlidesToShow($carousel);
            var hasDots = $carousel.hasClass("BannerCarousel--dots");
            var slickObjDefault = {
                infinite: true,
                dots: false,
                slidesToShow: slidesToShow.mb,
                slidesToScroll: 1,
                variableWidth: false,
                autoplay: true,
                autoplaySpeed: 7000,
                speed: 1000,
                fade: true,
                pauseOnHover: false,
                cssEase: 'linear',
                nextArrow: '<span class="BannerCarousel-arrow BannerCarousel-arrow--next appico-chevron-right"></span>',
                prevArrow: '<span class="BannerCarousel-arrow BannerCarousel-arrow--prev appico-chevron-left"></span>'
            };
            $carousel
                .on('setPosition', function () {
                $(this).find('.banner-carousel-content').height('auto');
                var slickTrack = $(this).find('.slick-track');
                var slickTrackHeight = $(slickTrack).height();
                $(this).find('.banner-carousel-content').css('height', slickTrackHeight + 'px');
            })
                .on('afterChange', function () {
                Core.TextTyping.init();
            })
                .slick(slickObjDefault);
        });
    },
    /**
     * Looks for a data attribute on the root element and uses it to change the slidesToShow value.
     * For example `data-slides-to-show="4,3,1"` would be 4 slides for desktop, 3 for tablet and 1 for mobile.
     */
    getCustomSlidesToShow: function ($carousel) {
        var dt = 3, tb = 2, mb = 1;
        // checks attribute exists
        var customSlidesToShow = $carousel.attr('data-slides-to-show');
        if (customSlidesToShow) {
            // splits the attribute values by comma
            var split = customSlidesToShow.split(",");
            // ensures desktop number is a valid number
            var newDt = parseInt(split[0], 10);
            var newDtIsValid = newDt.toString() !== "NaN";
            if (newDtIsValid)
                dt = tb = mb = newDt; // sets all just incase other values aren't valid
            else
                console.warn("BannerCarousel", "getCustomSlidesToShow", "Desktop value was invalid", customSlidesToShow);
            // ensures tablet number is a valid number
            var newTb = parseInt(split[1], 10);
            var newTbIsValid = newTb.toString() !== "NaN";
            if (newTbIsValid)
                tb = mb = newTb; // sets mobile too just incase mobile value not valid
            else
                console.warn("BannerCarousel", "getCustomSlidesToShow", "Tablet value was invalid", customSlidesToShow);
            // ensures tablet number is a valid number
            var newMb = parseInt(split[2], 10);
            var newMbIsValid = newMb.toString() !== "NaN";
            if (newMbIsValid)
                mb = newMb;
            else
                console.warn("BannerCarousel", "getCustomSlidesToShow", "Mobile value was invalid", customSlidesToShow);
        }
        return {
            dt: dt, tb: tb, mb: mb
        };
    }
};
Core.Button = {
    init: function () {
        Core.Button.Cache = {
            btnSubmitSearch: $('.search-form .search-submit'),
            btnSubmitComment: $('.comment-form .form-submit .submit')
        };
        this.toggler();
    },
    toggler: function () {
        Core.Button.Cache.btnSubmitSearch.each(function () {
            $(this).append('<i class="icon-search"></i>');
        });
        Core.Button.Cache.btnSubmitComment.replaceWith('<button name="submit" type="submit" id="submit" class="submit btn btn-icon my-2 my-sm-0 btn-warning btn-wide btn-pill transition-splash-hover">Post Comment<i class="icon-plus"></i></button>');
    }
};
Core.NavbarToggle = {
    init: function () {
        Core.NavbarToggle.Cache = {
            $button: $('.navbar-toggler')
        };
        this.toggler();
    },
    toggler: function () {
        Core.NavbarToggle.Cache.$button.on('click', function () {
            var $this = $(this);
            if ($this.hasClass('on'))
                $this.removeClass('on');
            else
                $this.addClass('on');
        });
    }
};
Core.Shuffle = {
    init: function () {
        Core.Shuffle.Cache = {
            $button: $('.portfolio-navs .btn')
        };
        this.shuffle();
    },
    shuffle: function () {
        var Shuffle = window.Shuffle;
        var element = document.querySelector('.portfolio-list');
        shuffleInstance = new Shuffle(element, {
            itemSelector: '.portfolio-item',
            filterMode: Shuffle.FilterMode.ALL,
            speed: 500
        });
        Core.Shuffle.Cache.$button.on('click', function (evt) {
            var btn = evt.currentTarget;
            var isActive = btn.classList.contains('btn-primary');
            var btnGroup = btn.getAttribute('data-group');
            console.log("Buttonnya", btn);
            Core.Shuffle._removeActiveClassFromChildren(btn.parentNode);
            var filterGroup;
            if (isActive) {
                btn.classList.remove('btn-primary');
                filterGroup = Shuffle.ALL_ITEMS;
            }
            else {
                btn.classList.add('btn-primary');
                filterGroup = btnGroup;
            }
            shuffleInstance.filter(filterGroup, function (element) {
                console.log(element);
                return element.getAttribute('data-groups');
            });
        });
        shuffleInstance.layout();
    },
    _removeActiveClassFromChildren: function (parent) {
        var children = parent.children;
        for (var i = children.length - 1; i >= 0; i--) {
            children[i].classList.remove('btn-primary');
        }
    }
};
Core.Testimonials = {
    init: function () {
        Core.Testimonials.Cache = {
            $content: $('.testimonials-inner')
        };
        this.initialize();
    },
    initialize: function () {
        Core.Testimonials.Cache.$content.each(function () {
            var $carousel = $(this);
            var slickObjDefault = {
                infinite: true,
                dots: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 5000,
                speed: 500,
                pauseOnHover: false,
                cssEase: 'linear',
                nextArrow: '<span class="testimonials-arrow testimonials-arrow--next appico-chevron-right"></span>',
                prevArrow: '<span class="testimonials-arrow testimonials-arrow--prev appico-chevron-left"></span>',
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            };
            $carousel
                .slick(slickObjDefault);
        });
    }
};
Core.TextTyping = {
    init: function () {
        Core.TextTyping.Cache = {
            $text: $('.text-typing')
        };
        this.typing();
    },
    typing: function () {
        $.each(Core.TextTyping.Cache.$text, function () {
            var $this = $(this);
            var i = 0;
            var txt = $this.text();
            var speed = 50;
            var newText = "";
            //setInterval(typeWriter, speed*txt.length);
            $this.html("");
            typeWriter();
            function typeWriter() {
                if (i < txt.length) {
                    newText += txt.charAt(i);
                    i++;
                    setTimeout(typeWriter, speed);
                }
                $this.html(newText);
            }
        });
    }
};
Core.WorkCarousel = {
    init: function () {
        Core.WorkCarousel.Cache = {
            $WorkCarousel: $('.work-carousel-wrap')
        };
        this.initialize();
    },
    initialize: function () {
        Core.WorkCarousel.Cache.$WorkCarousel.each(function () {
            var $carousel = $(this);
            var slidesToShow = Core.WorkCarousel.getCustomSlidesToShow($carousel);
            var hasDots = $carousel.hasClass("work-carousel--dots");
            var slickObjDefault = {
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: slidesToShow.dt,
                cssEase: 'linear',
                nextArrow: '<button class="work-carousel-arrow work-carousel-arrow--next icon-chevron-right"></button>',
                prevArrow: '<button class="work-carousel-arrow work-carousel-arrow--prev icon-chevron-left"></button>',
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: slidesToShow.tb,
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: slidesToShow.mb,
                            dots: true,
                            arrows: false
                        }
                    }]
            };
            $carousel
                .slick(slickObjDefault);
        });
    },
    /**
     * Looks for a data attribute on the root element and uses it to change the slidesToShow value.
     * For example `data-slides-to-show="4,3,1"` would be 4 slides for desktop, 3 for tablet and 1 for mobile.
     */
    getCustomSlidesToShow: function ($carousel) {
        var dt = 4, tb = 3, mb = 1;
        // checks attribute exists
        var customSlidesToShow = $carousel.attr('data-slides-to-show');
        if (customSlidesToShow) {
            // splits the attribute values by comma
            var split = customSlidesToShow.split(",");
            // ensures desktop number is a valid number
            var newDt = parseInt(split[0], 10);
            var newDtIsValid = newDt.toString() !== "NaN";
            if (newDtIsValid)
                dt = tb = mb = newDt; // sets all just incase other values aren't valid
            else
                console.warn("BannerCarousel", "getCustomSlidesToShow", "Desktop value was invalid", customSlidesToShow);
            // ensures tablet number is a valid number
            var newTb = parseInt(split[1], 10);
            var newTbIsValid = newTb.toString() !== "NaN";
            if (newTbIsValid)
                tb = mb = newTb; // sets mobile too just incase mobile value not valid
            else
                console.warn("BannerCarousel", "getCustomSlidesToShow", "Tablet value was invalid", customSlidesToShow);
            // ensures tablet number is a valid number
            var newMb = parseInt(split[2], 10);
            var newMbIsValid = newMb.toString() !== "NaN";
            if (newMbIsValid)
                mb = newMb;
            else
                console.warn("BannerCarousel", "getCustomSlidesToShow", "Mobile value was invalid", customSlidesToShow);
        }
        return {
            dt: dt, tb: tb, mb: mb
        };
    }
};
