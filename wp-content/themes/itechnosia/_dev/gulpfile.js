var path = require('path');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var browserSync   = require('browser-sync').create();
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var flatten = require('gulp-flatten');
var ts = require('gulp-typescript');
var handlebars = require('gulp-compile-handlebars');
var rename = require("gulp-rename");
var fs = require('fs');

var autoPrefixerBrowsers = [
    // Desktop
    'last 3 Chrome versions',
    'last 2 Firefox versions',
    'last 2 Safari versions',
    'last 2 Edge versions',
    'ie >= 9',
    // Mobile
    'last 3 ChromeAndroid versions',
    'last 3 Android versions',
    'last 3 FirefoxAndroid versions',
    'last 3 iOS versions',
    'last 2 ExplorerMobile versions',
    'last 2 OperaMobile versions'
    // Other
    , '> 2% in AU'
];

var BUILD = "../";

var config = {
    dest: {
        dir: '../*',
        js: '../js/',
        css: '../',
        images: '../img/',
        fonts: '../fonts/'
    },
    src: {
        js: 'scripts/',
        bower: "bower_components/",
        css: "scss/",
        images: "images/",
        fonts: "fonts/",
        templates: "templates/"
    }
};

gulp.task('default', ['build']);

gulp.task('build', ['js', 'css' /*, 'html'*/]);

// Lint JS
gulp.task('lint', function () {
    return gulp.src(config.src.js + '*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Clean output dir
gulp.task('clean', function () {
    console.log('cleaning ' + config.dest.dir);
    return gulp.src(config.dest.dir)
        .pipe(clean());
});

// Concat and minify libraries
gulp.task('js', ['lint', 'bundle-js']);

// build all JS bundles
gulp.task('bundle-js', ['build-bootstrap-bundle', 'build-libraries-bundle', 'build-app-bundle' /*,'minify-bundles'*/]);

// todo: replace with Angular Bootstrap libraries

// build bootstrap bundle
gulp.task('build-bootstrap-bundle', function () {
    return gulp.src([
            // Order is important         
          //config.src.bower + 'angular-bootstrap/ui-bootstrap.js',
          //config.src.bower + 'angular-bootstrap/ui-bootstrap-tpls.js'

          config.src.bower + 'bootstrap4/dist/js/bootstrap.js'       
    ])
      .pipe(concat('bootstrap-bundles.js'))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(config.dest.js));
});

// build library bundle
gulp.task('build-libraries-bundle', function () {
    return gulp.src([
            // Order is important
             config.src.bower + 'jquery/dist/jquery.js'
            , config.src.bower + 'jquery-ui/jquery-ui.js'         
            , config.src.bower + 'lodash/lodash.js'   
            , config.src.bower + 'slick-carousel/slick/slick.js' 
            , config.src.bower + 'swipebox/src/js/jquery.swipebox.js'                
            , config.src.bower + 'masonry/dist/masonry.pkgd.js'  
            , config.src.bower + 'shufflejs/dist/shuffle.js' 
            , config.src.bower + 'imagesloaded/imagesloaded.pkgd.js' 
            , config.src.bower + 'jquery_lazyload/jquery.lazyload.js' 
            , config.src.js + 'plugins/dzsparallaxer.js' 
                         
    ])
      .pipe(concat('libraries-bundles.js'))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(config.dest.js));
});

// build app bundle (our custom code)
gulp.task('build-app-bundle', function () {
    return gulp.src([            
          // Order is important
          config.src.js + 'core.js'
          , config.src.js + 'components.js'
          , config.src.js + 'core/*.js'
          , config.src.js + 'custom.js'
          //config.src.js + 'ready.js'
      
            
    ])
        .pipe(ts({
            noImplicitAny: false,
            noEmitOnError: true,
            removeComments: false,
            sourceMap: true,
            allowJs: true,
            target: "es5",
            outFile: 'app.js'
        }))
      //.pipe(concat('app.js'))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(config.dest.js));
});

// minify all bundles
gulp.task('minify-bundles', function () {
    gulp.src([
            config.dest.js + 'bootstrap-bundles.js',
            config.dest.js + 'libraries-bundles.js',
            config.dest.js + 'app.js'
    ])
        .pipe(uglify())
        .pipe(gulp.dest(config.dest.js));
});

// build the css
gulp.task('css', function () {
    return gulp.src(config.src.css + '*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(postcss([autoprefixer({
          add: false,
          browsers: autoPrefixerBrowsers,
          cascade: false
      })]))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(config.dest.css));
});



/*======================== Handle Handlebars ========================*/
gulp.task('html', function () {
  var templateData = ""; //JSON.parse(fs.readFileSync(config.src.variables + "dataClientsCompiled.js"));
  var options = {
    ignorePartials: true, //ignores the unknown footer2 partial in the handlebars template, defaults to false 
    partials : {
      footer : '<footer>the end</footer>'
    },
    batch : [
      config.src.templates + 'partials',
      config.src.templates + 'page-sections'

    ],
    helpers : {
      capitals : function(str){
        return str.toUpperCase();
      }
    }
  };

  //var data = require("_Dev/DATACLIENTS/dataClientsCompiled.js");
  //var data = JSON.parse(fs.readFileSync("_Dev/DATACLIENTS/dataClientsCompiled.js"));
  
  var src = [config.src.templates + "*.hbs", 
        "!" + config.src.templates + "partials/*.hbs",
        "!" + config.src.templates + "page-sections/*.hbs"];

  return gulp.src(src)
    //.pipe(handlebars(data))
    .pipe(handlebars(templateData, options))
    .pipe(rename(function(path) {
      path.extname = '.html';
    }))
    .pipe(flatten())
    .pipe(gulp.dest( BUILD ))
    .pipe(browserSync.stream());

});



gulp.task('watch', function () {
    gulp.watch(config.src.css + '**/*.scss', ['css']);
    gulp.watch([config.src.js + '*.js', config.src.js + '**/*.js'], ['build-app-bundle']);
    gulp.watch(config.src.templates + '**/*.hbs', ['html']);
});

gulp.task('watch-dev', function () {
    gulp.watch(config.src.css + '**/*.scss', ['css']);
    gulp.watch(config.src.js + '*.js', ['build-app-bundle']);
});