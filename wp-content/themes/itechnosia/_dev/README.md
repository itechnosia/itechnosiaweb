# Indocreativelab

Technologies used:
  - Gulp, NPM and Bower
  - Sass
  - NPM version 2.15.3 (npm install npm@2 -g)
  - handlebarsjs

## FREE images
Get free images from https://unsplash.com

## Installation from this path
- NodeJS and NPM
- Install Gulp CLI with `npm install gulp -g`
- Install Bower with `npm install bower -g`
- After installing NPM and Gulp, run the command `npm install`
then run the command `bower install`


## Compile Front End Assets and WATCH
- To compile SCSS and JS and Template documents, in your command line, and run `gulp` 
- To automatically WATCH for changes to SCSS and JS and Template documents, in your command line, and run `gulp watch` 


## Bootstrap Variables:
Please use "_dev/scss/frameworks/_customVariables.sass" as much as possible for basis 


## Icons
Uses Icomoon font icon. 

### Description
These are shortcut icons, used for favicons, plus Android, iOS & Windows launcher icons.
There are declared in meta tags in every single page *.hbs.

### Icomoon
Icomoon is a free online icon font generator, made from SVG files. The source files for our font icon are in `_dev/SVG/`.
If you need to add a new icon, open it in Illustrator, go to `Object->Artboards->Fit to Artwork bounds` (this ensures there's no white space around the icon). Then go to 'icomoon.io' and use the free app to upload a clean set. Make sure you select all the icons, then click 'Generate font'. Download and unpack the zip, change the `style.css` to `_dev/scss/vendor/_icomoon.scss` and replace the folder `_dev/fonts/icomoon/` with your new one. You'll also need to replace the path of the font to this such as `fonts/icomoon/` from `_dev/scss/vendor/_icomoon.scss`, so that it points to the `../fonts/icomoon/` directory.


## CSS
- Preprocessor used is Sass, using `.scss` syntax.
- Methodology used is SMACSS:
    - see online docs: 'https://smacss.com/book/'
    - YouTube also has loads of tutorial videos (TODO: need to make a list)
- Autoprefixer post-processor used to manage css vendor prefixes, so please don't include them in your scss files.


## Themes
All element color which using general colors (ie: $brand-primary / $color-primary, $brand-success  / $color-success, $brand-info  / $color-info, $brand-danger / color-danger, $border-color, $border-secondary, $color-secondary, etc...), please write it under `_dev/scss/helpers/_variables.scss` preferably in one line per each color's element.



## Working Folder

### Dev
`Dev` is working folder for development include assets such as images, themes, fonts, etc

### Prod
`Prod` is compiled folder, not allowed to touch or working here, because everytime gulp will replace all the data under this folder while `WATCH` or compiling 


## New Page
Create new page under `_dev/templates/` with format `*.hbs`, and it will automatically generate the same html file name under `Prod` while `WATCH`

