Core.Components.SetCache = function() {
    this.Cache = {
        button: document.querySelector('.btn'),
        bannerCarousel: document.querySelector('.banner-carousel'),
        navbarToggle: document.querySelector('.navbar-toggler'),
        shuffle: document.querySelector('.portfolio-home'),
        testimonials: document.querySelector('.testimonials'),
        textTyping: document.querySelector('.text-typing'),
        workCarousel: document.querySelector('.work-carousel'),
    };
};

Core.Components.init = function(){
    this.SetCache();


    /**
     * @see core/button.js
    */
    if (this.Cache.button) {
        Core.Button.init();
    }   

    /**
     * @see core/bannerCarousel.js
    */
    if (this.Cache.bannerCarousel) {
        Core.BannerCarousel.init();
    }     


    /**
     * @see core/navbarToggle.js
     */ 
    if (this.Cache.navbarToggle) {
        Core.NavbarToggle.init();
    } 


    /**
     * @see core/shuffle.js
    */
    if (this.Cache.shuffle) {
        Core.Shuffle.init();
    }   

    /**
     * @see core/testimonials.js
    */
    if (this.Cache.testimonials) {
        Core.Testimonials.init();
    }    


    /**
     * @see core/textTyping.js
    */
    if (this.Cache.textTyping) {
        Core.TextTyping.init();
    }      
       


    /**
     * @see core/workCarousel.js
    */
    if (this.Cache.workCarousel) {
        Core.WorkCarousel.init();
    }     
};


