(function(){
    var root = this;
    var Core = function(obj) {
        if (obj instanceof Core){
            return obj;
        }
        if (!(this instanceof Core)){
            return new Core(obj);
        }
    };

    root.Core = Core;

}).call(this);

Core.SetCache = function() {
    Core.Cache = {
        $window: $(window),
        $document: $(document),
        $body: $('body')

    };
};

Core.Components = function(){
    Core.Components.init();
};

Core.Config = Core.Config || {};

Core.IsMobile = function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (width <= Core.Config.MobileWidth) {
            return true;
        }
    }
    return false;
};

Core.IsTablet = function() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (width <= Core.Config.TabletWidth) {
            return true;
        }
    }
    return false;
};


Core.IsAppleTouch = function(){
    var ua = navigator.userAgent.toUpperCase();
    var 
      ipad = ua.indexOf('IPAD')>-1,
      ipod = ua.indexOf('IPOD')>-1,
      iphone = ua.indexOf('IPHONE')>-1 ;
    return   ipad || ipod || iphone ;
};

Core.CustomSelect = function(el, obj) {
    $(el).selectpicker(obj);
    $(el).selectpicker('setStyle', 'btn dropdown-toggle', 'remove');

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      $(el).selectpicker('mobile');
    }
};

Core.SumoSelectCustom = function(el, obj) {
    $(el).SumoSelect(obj);
};

Core.RemoveJSONMessage = function(){
    var $messageBox = $('.json-message');
    $messageBox.on('click', function(){
      this.remove();
    });
    setTimeout(function() {
        $messageBox.slideUp(500, function(){
          this.remove();
        });
    }, 10000);
};

Core.RenderTemplate = function(templateName, data, $placeholder){
    var source = $(templateName).html();
    var template = Handlebars.compile(source);

    $placeholder.html(template(data));
};

Core.GetParameterByName = function(name){
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

Core.UpdateUrlParameter = function (uri, key, value) {
    // remove the hash part before operating on the uri
    var i = uri.indexOf('#');
    var hash = i === -1 ? '' : uri.substr(i);
    uri = i === -1 ? uri : uri.substr(0, i);

    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        uri = uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        uri = uri + separator + key + "=" + value;
    }
    return uri + hash;  // finally append the hash as well
};

String.prototype.strim = function(needle) {
    var org = this;
    var modified = "";
    var first_pos = 0;
    var last_pos = org.length - 1;
    //find first non needle char position
    for (var i = 0; i < org.length; i++) {
        if (org.charAt(i) !== needle) {
            first_pos = (i == 0 ? 0 : i);
            break;
        }
    }
    // find first non needle char position
    for (var a = org.length - 1; a > 0; a--) {
        if (org.charAt(a) !== needle) {
            last_pos = (a == org.length ? org.length : i + 1);
            break;
        }
    }

    return org.substring(first_pos, last_pos);
};

Core.Loading = (function() {
    var initShow = function(){
        Core.Cache.$body.addClass("showLoading");
    };
    var initHide = function(){
        Core.Cache.$body.removeClass("showLoading");
    };
    return {
        Show: initShow,
        Hide: initHide
    };
})();


Core.ShowPageLoading = function(message){
    var msg = message || false;
        el = $('#loading-overlay');
    if(message) {
        if(el.length === 0) {
            if(msg != "")
                $('body').prepend('<div id="loading-overlay" class="loading-overlay has-message" style="display:block;"><span class="lv1"><span class="lv2">' + msg + '</span></span></div>');
            else
                $('body').prepend('<div id="loading-overlay" class="loading-overlay no-message" style="display:block;"></div>');        
        }else{
            if(msg != "")
                el
                    .addClass('has-message')
                    .removeClass('no-message')
                    .append('<span class="lv1"><span class="lv2">' + msg + '</span></span>')
                    .show();
            else
                el
                .addClass('no-message')
                .removeClass('has-message')
                .html("")
                .show()
                .end();
        }
    }else{
        Core.Cache.$body.addClass('showLoading');
    }
};

Core.HidePageLoading = function(){
    Core.Cache.$loadingOverlay.hide();
    $('#loading-overlay')
        .hide()
        .html("")
        .removeClass('no-message has-message');
    Core.Cache.$body.removeClass('showLoading');
};


/**
 * Mobile safari doesn't lose focus when tapping outside an input/textarea, so needs s workaround.
 * REF: https://stackoverflow.com/questions/30049222/iphone-keyboard-not-hiding-when-tapping-the-screen#answer-34944177
 */
Core.mobileSafariUnfocusWorkaround = function() {
    var cacheInput = null;
    var timer = null;
    if(!Core.IsAppleTouch()){
        return false;
    }
    Core.Cache.$document.on('focus','input',function(e){
        if($(e.target).closest(".selectize-input").length) return;
        cacheInput = e.target;
    });
    Core.Cache.$document.on('focus','textarea',function(e){
        if($(e.target).closest(".selectize-input").length) return;
        cacheInput = e.target;
    });
    Core.Cache.$document.on('touchend',function(e){
        if(e.target.tagName!=='INPUT'&&e.target.tagName!=='TEXTAREA'){
            if(cacheInput!==null){
                timer = setTimeout(function(){
                    cacheInput.blur();
                    clearTimeout(timer);
                },300);
            }
        }
    });
};

Core.SetCache();

Core.Cache.$document.ready(function () {
    Core.mobileSafariUnfocusWorkaround();
    //Core.Engine.init();
    Core.Components({});    
});
