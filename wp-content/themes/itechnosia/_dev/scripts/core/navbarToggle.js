Core.NavbarToggle = {
	init: function() {
		Core.NavbarToggle.Cache = {
			$button: $('.navbar-toggler')
		};

		this.toggler();
	},

	toggler: function() {
		Core.NavbarToggle.Cache.$button.on('click', function(){
			var $this = $(this);
			if ($this.hasClass('on')) 
				$this.removeClass('on');
  			else 
  				$this.addClass('on');
		});
	}
}