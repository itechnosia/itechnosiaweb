Core.BannerCarousel = {
    init: function () {
			Core.BannerCarousel.Cache = {
				$BannerCarousel: $('.banner-carousel-inner')
      };

        this.initialize();
    },

    initialize: function(){

		Core.BannerCarousel.Cache.$BannerCarousel.each(function(){
			var $carousel = $(this);
			
			var slidesToShow = Core.BannerCarousel.getCustomSlidesToShow( $carousel );
			var hasDots = $carousel.hasClass("BannerCarousel--dots");
		
			var slickObjDefault = {
				infinite: true,
				dots: false,
				slidesToShow: slidesToShow.mb,
				slidesToScroll: 1,
				variableWidth: false,
				autoplay: true,
  				autoplaySpeed: 7000,
  				speed: 1000,
				fade: true,
				pauseOnHover:false,
				cssEase: 'linear',
				nextArrow: '<span class="BannerCarousel-arrow BannerCarousel-arrow--next appico-chevron-right"></span>',			  
				prevArrow: '<span class="BannerCarousel-arrow BannerCarousel-arrow--prev appico-chevron-left"></span>'
			};

			$carousel
				.on('setPosition', function () {
					$(this).find('.banner-carousel-content').height('auto');
					var slickTrack = $(this).find('.slick-track');
					var slickTrackHeight = $(slickTrack).height();
					$(this).find('.banner-carousel-content').css('height', slickTrackHeight + 'px');
				})
				.on('afterChange', function(){
					Core.TextTyping.init();
				})
				.slick(slickObjDefault);
		});
	},
	
	/**
	 * Looks for a data attribute on the root element and uses it to change the slidesToShow value.
	 * For example `data-slides-to-show="4,3,1"` would be 4 slides for desktop, 3 for tablet and 1 for mobile.
	 */
	getCustomSlidesToShow: function($carousel) {
		var dt = 3, tb = 2, mb = 1;
		
		// checks attribute exists
		var customSlidesToShow = $carousel.attr('data-slides-to-show');
		if(customSlidesToShow) {
			
			// splits the attribute values by comma
			var split = customSlidesToShow.split(",");
			
			// ensures desktop number is a valid number
			var newDt = parseInt(split[0], 10);
			var newDtIsValid = newDt.toString() !== "NaN";
			if( newDtIsValid ) dt = tb = mb = newDt; // sets all just incase other values aren't valid
			else console.warn("BannerCarousel", "getCustomSlidesToShow", "Desktop value was invalid", customSlidesToShow);

			// ensures tablet number is a valid number
			var newTb = parseInt(split[1], 10);
			var newTbIsValid = newTb.toString() !== "NaN";
			if( newTbIsValid ) tb = mb = newTb; // sets mobile too just incase mobile value not valid
			else console.warn("BannerCarousel", "getCustomSlidesToShow", "Tablet value was invalid", customSlidesToShow);
			
			// ensures tablet number is a valid number
			var newMb = parseInt(split[2], 10);
			var newMbIsValid = newMb.toString() !== "NaN";
			if( newMbIsValid ) mb = newMb;
			else console.warn("BannerCarousel", "getCustomSlidesToShow", "Mobile value was invalid", customSlidesToShow);
		}

		return {
			dt: dt, tb: tb, mb: mb
		}
	}
}
