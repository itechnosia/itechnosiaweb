Core.WorkCarousel = {
    init: function () {
			Core.WorkCarousel.Cache = {
				$WorkCarousel: $('.work-carousel-wrap')
      };

       	this.initialize();
    },

    initialize: function(){

		Core.WorkCarousel.Cache.$WorkCarousel.each(function(){
			var $carousel = $(this);
			
			var slidesToShow = Core.WorkCarousel.getCustomSlidesToShow( $carousel );
			var hasDots = $carousel.hasClass("work-carousel--dots");
			var slickObjDefault = {
				dots: false,
				infinite: true,
				speed: 500,
				slidesToShow: slidesToShow.dt,
				cssEase: 'linear',
				nextArrow: '<button class="work-carousel-arrow work-carousel-arrow--next icon-chevron-right"></button>',			  
				prevArrow: '<button class="work-carousel-arrow work-carousel-arrow--prev icon-chevron-left"></button>',
				responsive: [
				{
				  breakpoint: 992,
				  settings: {
				    slidesToShow: slidesToShow.tb,
				  }
				},
				{
				  breakpoint: 576,
				  settings: {
				    slidesToShow: slidesToShow.mb,
				    dots: true,
				    arrows: false
				  }
				}]
			};

			$carousel
				/*
				.on('setPosition', function () {
					$(this).find('.banner-carousel-content').height('auto');
					var slickTrack = $(this).find('.slick-track');
					var slickTrackHeight = $(slickTrack).height();
					$(this).find('.banner-carousel-content').css('height', slickTrackHeight + 'px');
				})
				.on('afterChange', function(){
					Core.TextTyping.init();
				})
				*/
				.slick(slickObjDefault);
		});
	},
	
	/**
	 * Looks for a data attribute on the root element and uses it to change the slidesToShow value.
	 * For example `data-slides-to-show="4,3,1"` would be 4 slides for desktop, 3 for tablet and 1 for mobile.
	 */
	getCustomSlidesToShow: function($carousel) {
		var dt = 4, tb = 3, mb = 1;
		
		// checks attribute exists
		var customSlidesToShow = $carousel.attr('data-slides-to-show');
		if(customSlidesToShow) {
			
			// splits the attribute values by comma
			var split = customSlidesToShow.split(",");
			
			// ensures desktop number is a valid number
			var newDt = parseInt(split[0], 10);
			var newDtIsValid = newDt.toString() !== "NaN";
			if( newDtIsValid ) dt = tb = mb = newDt; // sets all just incase other values aren't valid
			else console.warn("BannerCarousel", "getCustomSlidesToShow", "Desktop value was invalid", customSlidesToShow);

			// ensures tablet number is a valid number
			var newTb = parseInt(split[1], 10);
			var newTbIsValid = newTb.toString() !== "NaN";
			if( newTbIsValid ) tb = mb = newTb; // sets mobile too just incase mobile value not valid
			else console.warn("BannerCarousel", "getCustomSlidesToShow", "Tablet value was invalid", customSlidesToShow);
			
			// ensures tablet number is a valid number
			var newMb = parseInt(split[2], 10);
			var newMbIsValid = newMb.toString() !== "NaN";
			if( newMbIsValid ) mb = newMb;
			else console.warn("BannerCarousel", "getCustomSlidesToShow", "Mobile value was invalid", customSlidesToShow);
		}

		return {
			dt: dt, tb: tb, mb: mb
		}
	}
}
