Core.TextTyping = {
	init: function() {
		Core.TextTyping.Cache = {
			$text: $('.text-typing')
		};

		this.typing();
	},

	typing: function() {
		$.each(Core.TextTyping.Cache.$text, function(){
			var $this = $(this);
			var i = 0;
			var txt = $this.text();
			var speed = 50; 
			var newText = "";

			//setInterval(typeWriter, speed*txt.length);
			$this.html("");
			typeWriter();

			function typeWriter() {
			  if (i < txt.length) {
			    newText += txt.charAt(i);
			    i++;			    
			    setTimeout(typeWriter, speed);
			  }

			  $this.html(newText);
			}
		});
	}
}