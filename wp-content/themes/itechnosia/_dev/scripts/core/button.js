Core.Button = {
	init: function() {
		Core.Button.Cache = {
			btnSubmitSearch: $('.search-form .search-submit'),
			btnSubmitComment: $('.comment-form .form-submit .submit')
		};

		this.toggler();
	},

	toggler: function() {
		Core.Button.Cache.btnSubmitSearch.each( function(){
			$(this).append('<i class="icon-search"></i>');
		});

		Core.Button.Cache.btnSubmitComment.replaceWith('<button name="submit" type="submit" id="submit" class="submit btn btn-icon my-2 my-sm-0 btn-warning btn-wide btn-pill transition-splash-hover">Post Comment<i class="icon-plus"></i></button>');
	}
}