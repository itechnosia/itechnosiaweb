Core.Shuffle = {
	init: function() {
		Core.Shuffle.Cache = {
			$button: $('.portfolio-navs .btn')
		};

		this.shuffle();
	},

	shuffle: function() {
		var Shuffle = window.Shuffle;
		var element = document.querySelector('.portfolio-list');

		shuffleInstance = new Shuffle(element, {
		  itemSelector: '.portfolio-item',
		  filterMode: Shuffle.FilterMode.ALL,
		  speed: 500
		});

		Core.Shuffle.Cache.$button.on('click', function(evt){
			const btn = evt.currentTarget;
    		const isActive = btn.classList.contains('btn-primary');
    		const btnGroup = btn.getAttribute('data-group');

			console.log("Buttonnya", btn);
    		
    		Core.Shuffle._removeActiveClassFromChildren(btn.parentNode);
    
		    let filterGroup;
		    if (isActive) {
		      btn.classList.remove('btn-primary');
		      filterGroup = Shuffle.ALL_ITEMS;
		    } else {
		      btn.classList.add('btn-primary');
		      filterGroup = btnGroup;
		    }

			shuffleInstance.filter(filterGroup, function (element) {
				console.log(element);
			  return element.getAttribute('data-groups');
			});
		});
        shuffleInstance.layout();
	},



	_removeActiveClassFromChildren: function(parent) {
	    const { children } = parent;
	    for (let i = children.length - 1; i >= 0; i--) {
	      children[i].classList.remove('btn-primary');
	    }
	  }

}