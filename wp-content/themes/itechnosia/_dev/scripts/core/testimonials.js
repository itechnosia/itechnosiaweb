Core.Testimonials = {
    init: function () {
			Core.Testimonials.Cache = {
				$content: $('.testimonials-inner')
      };

        this.initialize();
    },

    initialize: function(){

		Core.Testimonials.Cache.$content.each(function(){
			var $carousel = $(this);
			
			var slickObjDefault = {
				infinite: true,
				dots: true,
				slidesToShow:  2,
				slidesToScroll: 1,
				autoplay: true,
  				autoplaySpeed: 5000,
  				speed: 500,
				pauseOnHover:false,
				cssEase: 'linear',
				nextArrow: '<span class="testimonials-arrow testimonials-arrow--next appico-chevron-right"></span>',			  
				prevArrow: '<span class="testimonials-arrow testimonials-arrow--prev appico-chevron-left"></span>',				
				responsive: [
					{
					  breakpoint: 767,
					  settings: {
					    slidesToShow: 1,
					    slidesToScroll: 1
					  }
					}
				]
			};

			$carousel
			/*
				.on('setPosition', function () {
					$(this).find('.testimonials-quote').height('auto');
					var slickTrack = $(this).find('.slick-track');
					var slickTrackHeight = $(slickTrack).height();
					$(this).find('.testimonials-quote').css('height', slickTrackHeight + 'px');
				})
				*/
				.slick(slickObjDefault);
		});
	}
}
