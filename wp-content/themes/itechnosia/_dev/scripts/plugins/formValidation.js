;(function($) {
  var pluginName = 'formValidation';
 
  function Plugin(element, options) {
    var el = element;
    var $el = $(element);
 
    options = $.extend({}, $.fn[pluginName].defaults, options);
 
    function init() {
      // Add any initialization logic here...
       function checkAttrubute (attr) {
          if (typeof attr !== typeof undefined && attr !== false) {
              return true;
          }else{
              return false;
          }
      }

      $el.on('click', function(e){
        var status = true,
        $form = $(this).parents('.form-wrapper'),
        $control = $form.find('.form-control'),
        $radio = $form.find('.radio'),
        $checkbox = $form.find('.checkbox');

        // cleanup the form
        $form.find('.error').removeClass('error');

        //form control
        for(var a=0; a < $control.length; a++) {
            var $el  = $($control[a]);
            if($el.closest('.form-item').is(":visible")){ 
                if (checkAttrubute($el.attr('required'))) {
                    if($el.val() === "" || $el.val() === null ) {
                        status = false;
                        $el.closest('.form-item').addClass('error');
                    }else{
                        // validate email
                        if($el.attr('type') === "email") {
                            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                            if (!re.test($el.val())) {
                                status = false;
                                $el.closest('.form-item').addClass('error');
                            }
                        }

                        // validate range
                        if($el.hasClass('rangeValidation')) {
                            if (checkAttrubute($el.attr('data-min-value'))) {
                                if ( parseInt($el.val()) < $el.data('min-value') ) {
                                    status = false;
                                    $el.closest('.form-item').addClass('error');
                                }
                            }
                        }

                        // validate on relates 
                        if(checkAttrubute($el.attr('data-relates'))){
                            var $relates = $el.data('relates');

                            if($el.val() === $($relates).val()) {
                                status = false;
                                $($relates).closest('.form-item').addClass('error');
                            }
                        }
                    }
                }
            }
        }

        // radio button
        for(var b=0; b < $radio.length; b++) {
            var $el_radio         = $($radio[a]),
                $input      = $el_radio.find('input[type="radio"]'),
                $wrapper    = $el_radio.closest('.form-item'),
                isRequired  = false;


            if($wrapper.is(":visible")){ 

                for(var c=0; c < $input.length; c++) {
                    if (checkAttrubute( $($input[b]).attr('required') )) {
                        isRequired = true;
                    }
                }

                if(isRequired) {
                    if(typeof $el_radio.find('input[type="radio"]:checked').val() === typeof undefined) {
                        $wrapper.addClass('error');
                        status = false;
                    }
                }
            }
        }

        // checkbox
        for(var d=0; d < $checkbox.length; d++) {
            var $el_checkbox         = $($checkbox[d]),
                $wrapper_checkbox    = $el_checkbox.closest('.form-item');


               
            if($wrapper_checkbox.is(":visible")){  
                var $input_checkbox      = $el_checkbox.find('input[type="checkbox"]');   
                
                if (checkAttrubute($input_checkbox.attr('required'))) {
                    if($input_checkbox.prop("checked" ) === false) {
                        $wrapper_checkbox.addClass('error');
                        status = false;
                    }
                }
            }
        }



        if(!status){
            e.preventDefault();     
            e.stopPropagation();  
            var body = $("html, body"),
                getScrollTop = $form.find(".error:first").offset().top - 90;

                getScrollTop = getScrollTop > 0 ? getScrollTop : 0;

            body.stop().animate({scrollTop:getScrollTop}, '500', 'swing');

        }

        //================================== form error remove ===============================  
        $('.form-control').on('focus', function(){
            $(this).parents('.error').removeClass('error');
        });

        $('.radio label, .bootstrap-select.form-control .dropdown-toggle, .checkbox-list label, .checkbox-list input[type="checkbox"], .checkbox label, .checkbox input[type="checkbox"]').on('click',function(){
            $(this).parents('.error').removeClass('error');
        });

        //================================== form show hide =============================== 

        $('.radio.showhide').each(function(){
            var $this   = $(this),
                $input  = $this.find('input[type="radio"]');

            $input.on("click", function(){
                if($(this).val() === $this.data('default')){

                    if (checkAttrubute($this.attr('data-opposite'))) {
                        $($this.data('opposite')).collapse('hide');
                    }
                    
                    $($this.data('target')).collapse('show');

                } else {                
                     if (checkAttrubute($this.attr('data-opposite'))) {
                        $($this.data('opposite')).collapse('show');
                    }
                    
                    $($this.data('target')).collapse('hide');
                }
            });

         });



      });
 
      hook('onInit');
    }
 
    function fooPublic() {
      // Code goes here...
    }
 
    function option (key, val) {
      if (val) {
        options[key] = val;
      } else {
        return options[key];
      }
    }
 
    function destroy() {
      $el.each(function() {
        var el = this;
        var $el = $(this);
 
        // Add code to restore the element to its original state...
 
        hook('onDestroy');
        $el.removeData('plugin_' + pluginName);
      });
    }
 
    function hook(hookName) {
      if (options[hookName] !== undefined) {
        options[hookName].call(el);
      }
    }
 
    init();
 
    return {
      option: option,
      destroy: destroy,
      fooPublic: fooPublic
    };
  }
 
  $.fn[pluginName] = function(options) {
    if (typeof arguments[0] === 'string') {
      var methodName = arguments[0];
      var args = Array.prototype.slice.call(arguments, 1);
      var returnVal;
      this.each(function() {
        if ($.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function') {
          returnVal = $.data(this, 'plugin_' + pluginName)[methodName].apply(this, args);
        } else {
          throw new Error('Method ' +  methodName + ' does not exist on jQuery.' + pluginName);
        }
      });
      if (returnVal !== undefined){
        return returnVal;
      } else {
        return this;
      }
    } else if (typeof options === "object" || !options) {
      return this.each(function() {
        if (!$.data(this, 'plugin_' + pluginName)) {
          $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
        }
      });
    }
  };
 
  $.fn[pluginName].defaults = {
    onInit: function() {},
    onDestroy: function() {}
  };
 
})(jQuery);