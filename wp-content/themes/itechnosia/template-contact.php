<?php
/**
 * Template Name: Contact Template
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */
?>

	<?php get_header(); ?>

	<?php get_template_part( 'template-parts/subpage-banner', 'page' ); ?>


	<section class="page-section contact-info">
	    <div class="container-fluid">
	        <div class="contact-info-inner row no-gutters text-center">

	            <div class="contact-info-block col-sm-6 col-lg-3">
	                <div class="contact-info-icon">
	                    <span class="contact-info-icon-main icon-location2"></span>
	                    <span class="contact-info-icon-frame icon-map2"></span>
	                </div>
	                <h2 class="contact-info-title h6 mb-0">Address</h2>
	                <p class="contact-info-detail mb-0">Graha DLA - Jl. Otto Iskandar Dinata 392 - Bandung</p>
	            </div>

	            <div class="contact-info-block col-sm-6 col-lg-3">
	                <div class="contact-info-icon">
	                    <span class="contact-info-icon-main icon-quill"></span>
	                    <span class="contact-info-icon-frame icon-envelop"></span>
	                </div>
	                <h2 class="contact-info-title h6 mb-0">Email</h2>
	                <p class="contact-info-detail mb-0">info@indocreativelab.com</p>
	            </div>

	            <div class="contact-info-block col-sm-6 col-lg-3">
	                <div class="contact-info-icon">
	                    <span class="contact-info-icon-main icon-phone-hang-up"></span>
	                    <span class="contact-info-icon-frame icon-history"></span>
	                </div>
	                <h2 class="contact-info-title h6 mb-0">Phone Number</h2>
	                <p class="contact-info-detail mb-0">+(62) 22 4282 6007</p>
	            </div>

	            <div class="contact-info-block col-sm-6 col-lg-3">
	                <div class="contact-info-icon">
	                    <span class="contact-info-icon-main icon-printer"></span>
	                    <span class="contact-info-icon-frame icon-copy"></span>
	                </div>
	                <h2 class="contact-info-title h6 mb-0">Fax</h2>
	                <p class="contact-info-detail mb-0">+(62) 22 4282 6007</p>
	            </div>


	        </div>
	    </div>
	</section>

	<hr class="divider">

	<section class="page-section contact-form">
	  <div class="container-fluid">
	      <h2 class="page-section-title  text-center text-primary">Contact <span>Us</span></h2>
	      <p class="page-section-subtitle text-center">Hire us for your next project ?</p>
	    
	  </div>
	</section>

	<section class="page-section contact-location">
	  <div id="map" class="contact-map">
	    
	  </div>
	  
	  <figure class="bg-wave bg-pos-bottom-wave">
	    <img class="js-svg-injector" src="<?php bloginfo('template_directory');?>/img/bg/wave-bottom.svg" alt="wave background">
	  </figure>
	</section>

	<script>
      function initMap() {
        // to change google map styles https://snazzymaps.com/
        var styles =  [
            {
                "featureType": "all",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#00a57d"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -70
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "saturation": -60
                    }
                ]
            }
        ];

        var uluru = {lat: -6.927618, lng: 107.603456};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: uluru
        });
        map.setOptions({styles: styles});

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading text-center"><img src="img/indocreativelab-logo-small.png" width="100px" style="margin:auto;"></h1>'+
            '<div id="bodyContent" class="text-center">'+
            '<p><b>indocreativelab</b>, Graha DLA - Jl. Otto Iskandar Dinata 392 - Bandung</p> ' +
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 300
        });

        var image = 'img/logo-32x32.png';
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'IndoCreativeLab',
          icon: image
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbwZtfuK3JvDIbZ43aw80Dfl88vY23_Rk&callback=initMap">
    </script> 
	

	<?php get_footer(); ?>

    