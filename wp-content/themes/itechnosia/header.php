<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */

// ITCHNOSIA General Options
  $itechnosia_opts                     	= get_option( 'itechnosia_name' );
  $GLOBALS["admin_gmap_latitude"]       = $itechnosia_opts['admin_gmap_latitude'];
  $GLOBALS["admin_gmap_longitude"]      = $itechnosia_opts['admin_gmap_longitude'];
  $GLOBALS["admin_telephone"]           = $itechnosia_opts['admin_telephone'];
  $GLOBALS["admin_handphone"]           = $itechnosia_opts['admin_handphone'];
  $GLOBALS["admin_email"]               = $itechnosia_opts['admin_email'];
  $GLOBALS["admin_facebook"]            = $itechnosia_opts['admin_facebook'];
  $GLOBALS["admin_twitter"]             = $itechnosia_opts['admin_twitter'];
  $GLOBALS["admin_youtube"]             = $itechnosia_opts['admin_youtube'];
  $GLOBALS["admin_vimeo"]               = $itechnosia_opts['admin_vimeo'];
  $GLOBALS["admin_instagram"]           = $itechnosia_opts['admin_instagram'];
  $GLOBALS["admin_linkedin"]            = $itechnosia_opts['admin_linkedin'];
  $GLOBALS["admin_behance"]             = $itechnosia_opts['admin_behance'];

?>

<!DOCTYPE html>
<head>
   		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<?php wp_head(); ?>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="keywords" content="">
        <meta name="author" content="ITECHNOSIA.com">
        
         <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css">
    
    
        <link href="<?php bloginfo('template_directory');?>/style.css" rel="stylesheet" type="text/css">
        
    
        <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory');?>/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_directory');?>/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory');?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory');?>/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory');?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php bloginfo('template_directory');?>/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php bloginfo('template_directory');?>/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->    
  </head>
  <body <?php body_class(); ?>>  

      <div class="top-bar">  
        <div class="container-fluid">       
            <ol class="navbar-top">
              <li>
                <a href="" class="icon-instagram"></a>
              </li>
              <li>
                <a href="" class="icon-whatsapp"></a>
              </li>
              <li>
                <a href="" class="icon-facebook"></a>
              </li>
              <li>
                <a href="" class="icon-twitter"></a>
              </li>
            </ol>  
        </div>
      </div>
      
      <header id="header">
            <nav class="navbar navbar-expand-lg">
              <div class="container-fluid">            
      
                  <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">ITECHNOSIA</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>              
                  </button>
                  <div class="collapse navbar-collapse" id="navbarCollapse">
                    <?php 
			            $menu_args =  array( 
			              'menu' => 'main-menu', 
			              'container' => '', 
			              'container_class' => '', 
			              'container_id' => 'primaryMenu', 
			              'menu_class' => 'navbar-nav ml-auto', 
			              'menu_id' => '',
			              'echo' => true, 
			              'fallback_cb' => 'wp_page_menu', 
			              'before' => '', 'after' => '', 
			              'link_before' => '', 
			              'link_after' => '', 
			              'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			              'depth' => 0, 
			              'walker' => '', 
			              'theme_location' => 'primary');

			            wp_nav_menu( $menu_args); ?>
			            
                    <a href="tel:0817432107" class="btn btn-icon my-2 my-sm-0 btn-warning btn-wide btn-pill transition-splash-hover"><i class="icon-phone"></i>Start Your Project With Us</a>
                  </div>
      
              </div>
            </nav>
          </header>


      <main id="main">