<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */
?>

		      </main>


      <footer id="footer">
        <div class="container-fluid">
          <div class="footer-block pb-4">
              <div class="row">
                  <div class="col-sm-6">
                    
                  </div>
                  <div class="col-sm-6 text-sm-right">
                    <ul class="footer-social">
                      <li>
                        <a href="" class="icon-instagram"></a>
                      </li>
                      <li>
                        <a href="" class="icon-whatsapp"></a>
                      </li>
                      <li>
                        <a href="" class="icon-facebook"></a>
                      </li>
                      <li>
                        <a href="" class="icon-twitter"></a>
                      </li>
                    </ul>
                  </div>
              </div>
          </div>
      
          <div class="footer-block bd-top-1 space-1 pt-4 pb-5">
              <div class="row">
                  <div class="col-sm-4">
                      <span class="footer-logo">
                        <img src="<?php bloginfo('template_directory');?>/img/indocreativelab-logo-small.png">
                      </span>
                      <p class="small text-muted pt-2">Copyrights © 2019. All rights reserved.</p>
                  </div>
                  <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-6">
                          <h4 class="h6 font-weight-semi-bold">Quick links</h4>
                          <!-- List Group -->
                          
                          <?php if ( has_nav_menu( 'primary' ) ) : ?>
        					<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'advokatdk' ); ?>">
        						<?php
        							wp_nav_menu(
        								array(
        									'theme_location' => 'primary',
        									'menu_class'     => 'footer-nav',
        								)
        							);
        						?>
        					</nav><!-- .main-navigation -->
        				<?php endif; ?>

                          <!-- End List Group -->
                        </div>
                        <div class="col-sm-6">
                          <h4 class="h6 font-weight-semi-bold">Latest news</h4>
                          <!-- List Group -->
                          <ul class="footer-menu">
                            <li><a href="#">Account</a></li>
                            <li><a href="#">My tasks</a></li>
                            <li><a href="#">Projects</a></li>
                          </ul>
                          <!-- End List Group -->
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-4 text-sm-right ">
      
                      <h4 class="h6 font-weight-semi-bold mb-4">Any further question ? <br>Please call us now.</h4>
      
                      <a href="tel:0817432107" class="btn btn-success btn-icon my-2 my-sm-0 btn btn-primary btn-wide btn-pill transition-splash-hover"><i class="icon-phone"></i>+62 22 234 567 890</a>
                  </div>
              </div>
          </div>
      
      
        </div>
      </footer>
      <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/libraries-bundles.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/bootstrap-bundles.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/app.js"></script>
      <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c5dd7cd8579b60011d06c8d&product='inline-share-buttons' async='async'></script>

  </body>
</html>
