<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Itech_Nosia
 * @since ITECHNOSIA 1.0
 */

get_header(); ?>


<section class="page-section page-columns"> 
    <div class="container">

    	<?php if ( have_posts() ) : ?>

        <div class="row">
            <div class="col-lg-8 page-column page-column-left">
            	
                <h1 class="page-title h1 font-weight-semi-bold">News</h1>

                <div class="blog-list  card-columns">

                	<?php
                		while ( have_posts() ) : the_post();

		    				get_template_part( 'template-parts/content', get_post_format() );

						endwhile;
                	?>


                </div>

                <?php

                the_posts_pagination( array(
					'prev_text'          => __( 'Previous page', 'itechnosia' ),
					'next_text'          => __( 'Next page', 'itechnosia' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'itechnosia' ) . ' </span>',
				) );
				?>


            </div>
            <div class="col-lg-4 page-column page-column-right">
                <div class="sticky-top">

                	<?php get_sidebar(); ?>


                	<?php
						$content = get_post_field('post_content', $recent["ID"]);
						$content = strip_tags($content);
						echo substr($content, 0, 10);
                	?>

                <div class="sidepanel-block">   
                   <div class="inner">                           
                      <h4 class="sidepanel-block-label h6">CATEGORIES</h4>
                      <ul class="list-unstyled blog-category-list">
                        <li>
                            <a href="#" title=""><span>Apartments</span></a>
                            <span>7</span>
                        </li>
                        <li>
                            <a href="#" title=""><span>Business</span></a>
                            <span>15</span>
                        </li>
                        <li>
                            <a href="#" title=""><span>Construction</span></a>
                            <span>4</span>
                        </li>
                        <li>
                            <a href="#" title=""><span>Location</span></a>
                            <span>1</span>
                        </li>
                    </ul>
                    </div>
                  </div>


                  <div class="sidepanel-block">  
                   <div class="inner">         
                    <h4 class="sidepanel-block-label h6">POPULAR POSTS</h4> 
                    <ul class="list-unstyled popular-post-list">
                      <?php 
                        $popularpost = new WP_Query( array( 'posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
                        while ( $popularpost->have_posts() ) : $popularpost->the_post();

                          ?>
                          <li class="media">
                               <?php if ( has_post_thumbnail() ) : ?>
                                <figure class="popular-post-image mr-3" width="64" height="auto">
                                <?php
                                   echo get_the_post_thumbnail(get_the_ID(), 'thumbnail', NULL);
                                ?>                    
                                </figure>

                              <?php endif;?>

                            <div class="media-body">
                              <?php the_title( sprintf( '<h5 class="mt-0 mb-1"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>' ); ?>
                              <span><i class="icon-calendar"></i><?php echo get_the_date();?></span>
                            </div>
                          </li>
                          <?php
                         
                        endwhile;
                        ?>
                      </ul>
                  </div>
                </div>


                  <div class="sidepanel-block">  
                   <div class="inner">         
                    <h4 class="sidepanel-block-label h6">POPULAR POSTS</h4>  
                    <ul class="list-unstyled popular-post-list">
                      <li class="media">

                        <svg class="bd-placeholder-img mr-3" width="64" height="64" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 64x64"><title>Placeholder</title><rect fill="#868e96" width="100%" height="100%"></rect><text fill="#dee2e6" dy=".3em" x="50%" y="50%">64x64</text></svg>

                        <div class="media-body">
                          <h5 class="mt-0 mb-1">List-based media object</h5>
                          <span><i class="icon-calendar"></i>April 25, 2018</span>
                        </div>
                      </li>
                      <li class="media my-4">

                        <svg class="bd-placeholder-img mr-3" width="64" height="64" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 64x64"><title>Placeholder</title><rect fill="#868e96" width="100%" height="100%"></rect><text fill="#dee2e6" dy=".3em" x="50%" y="50%">64x64</text></svg>

                        <div class="media-body">
                          <h5 class="mt-0 mb-1">List-based media object</h5>
                          <span><i class="icon-calendar"></i>April 25, 2018</span>
                        </div>
                      </li>
                      <li class="media">
                        
                        <svg class="bd-placeholder-img mr-3" width="64" height="64" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 64x64"><title>Placeholder</title><rect fill="#868e96" width="100%" height="100%"></rect><text fill="#dee2e6" dy=".3em" x="50%" y="50%">64x64</text></svg>

                        <div class="media-body">
                          <h5 class="mt-0 mb-1">List-based media object</h5>
                          <span><i class="icon-calendar"></i>April 25, 2018</span>
                        </div>
                      </li>
                    </ul> 
                  </div>

                  </div>


                  <div class="sidepanel-block">  
                   <div class="inner">             
                    <h4 class="sidepanel-block-label h6">POPULAR TAGS</h4>
                    <span class="u-label u-label--sm u-label--primary mb-2">Application</span> <span class="u-label u-label--sm u-label--primary mb-2">Website</span> <span class="u-label u-label--sm u-label--primary mb-2">Design</span>
                  </div>
                  </div>


                </div>
            </div>
        </div>

        <?php
        else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>



    </div>
</section>


<?php get_template_part( 'template-parts/foot', 'gradient' ); ?>
<?php get_footer(); ?>
