<?php
/*
Plugin Name: DP Partner
Plugin URI: http://depewebdesign.com
Description: This plugin developed for showing Partner Member listing on your website
Version: 1
Author: Denden Permana
Author URI: http://depewebdesign.com
Text Domain: dp-partner
License: GPLv2
*/


////////////////////////////
// Custom Post Type Setup
////////////////////////////
add_action( 'init', 'partner_post_type' );
function Partner_post_type() {
	$labels = array(
		'name' => __('Member Partner', 'dp-partner'),
		'singular_name' => __('youtube Video', 'dp-partner'),
		'add_new' => __('Add New', 'dp-partner'),
		'add_new_item' => __('Add New Member Partner', 'dp-partner'),
		'edit_item' => __('Edit Member Partner', 'dp-partner'),
		'new_item' => __('New Member Partner', 'dp-partner'),
		'view_item' => __('View Member Partner', 'dp-partner'),
		'search_items' => __('Search Member Partner', 'dp-partner'),
		'not_found' => __('No member Partner on the list', 'dp-partner'),
		'not_found_in_trash' => __('No member Images found in Trash', 'dp-partner'),
		'parent_item_colon' => '',
		'menu_name' => __('Partner List', 'dp-partner')
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 21,
		'menu_icon' => 'dashicons-format-aside',
		'supports' => array('title', 'editor', 'thumbnail')
	); 

	register_post_type('partner', $args);
}


// Extra admin field for Partner Position
function Partner_position(){
  global $post;
  $custom = get_post_custom($post->ID);
  $partner_position = isset($custom['partner_position']) ?  $custom['partner_position'][0] : '';
  ?>
  <label><?php _e('Position', 'dp-partner'); ?>:</label>
  <input type="text" name="partner_position" value="<?php echo $partner_position; ?>" /> 
  <?php
}

function Partner_position_admin_init_custpost(){
	add_meta_box("partner_position", "Partner Position", "partner_position", "partner", "normal", "low");
}

add_action("add_meta_boxes", "partner_position_admin_init_custpost");

// Extra admin field for Partner email
function Partner_email(){
  global $post;
  $custom = get_post_custom($post->ID);
  $partner_email = isset($custom['partner_email']) ?  $custom['partner_email'][0] : '';
  ?>
  <label><?php _e('Email', 'dp-partner'); ?>:</label>
  <input type="text" name="partner_email" value="<?php echo $partner_email; ?>" /> 
  <?php
}

function Partner_email_admin_init_custpost(){
	add_meta_box("partner_email", "Partner Email", "partner_email", "partner", "normal", "low");
}

add_action("add_meta_boxes", "partner_email_admin_init_custpost");


// Save Extra Admin Fields
function Partner_mb_save_details(){
	global $post;
	if (isset($_POST["partner_position"])) {
		update_post_meta($post->ID, "partner_position", $_POST["partner_position"]);
	}

	if (isset($_POST["partner_email"])) {
		update_post_meta($post->ID, "partner_email", $_POST["partner_email"]);
	}
}

add_action('save_post', 'partner_mb_save_details');


// Add column in admin list view to show featured image
// http://wp.tutsplus.com/tutorials/creative-coding/add-a-custom-column-in-posts-and-custom-post-types-admin-screen/
function Partner_get_featured_image($post_ID) {  
	$post_thumbnail_id = get_post_thumbnail_id($post_ID);  
	if ($post_thumbnail_id) {  
		$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');  
		return $post_thumbnail_img[0];  
	}  
}

function Partner_columns_head($defaults) {  
	$defaults['featured_image'] = __('Featured Image', 'dp-partner');  
	return $defaults;  
}  

function Partner_columns_content($column_name, $post_ID) {  
	if ($column_name == 'featured_image') {  
		$post_featured_image = Partner_get_featured_image($post_ID);  
		if ($post_featured_image) {  
			echo '<a href="'.get_edit_post_link($post_ID).'"><img width="150" src="' . $post_featured_image . '" /></a>';  
		}  
	}  
}
add_filter('manage_partner_posts_columns', 'partner_columns_head');  
add_action('manage_partner_posts_custom_column', 'partner_columns_content', 10, 2);



//shortcode
add_shortcode('dp-partner', 'partner_shortcode');
function Partner_shortcode(){
	$args = array( 'post_type' => 'partner', 'posts_per_page' => '-1', 'orderby' => 'menu_order', 'order' => 'DESC');	
	$loop = new WP_Query( $args );	
	$partner = array();
	while ( $loop->have_posts() ) {
		$loop->the_post();
		$post_ID 			= get_the_ID();
		$title 				= get_the_title();
		$image 				= get_the_post_thumbnail( $post_ID , 'full' );			
		$post_thumbnail_id 	= get_post_thumbnail_id($post_ID);  
		$src 				= wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview'); 
		$partner[] 			= array(
			'title' 	=> $title, 
			'content' 	=> $content, 
			'image' 	=> $image, 
			'src' 		=> $src);
	}
	
	
	
	if(count($partner) > 0){?>
		<section class="page-section ps-partners bg-lightgray ">
			<div class="container">
				<h2 class="page-section-title  text-center">Our <span>CMS</span> Technologies</h2>
				<p class="page-section-subtitle text-center">We are provides the most powerful and valuable CMS in PHP and dot net.</p>
				<div class="partners">
					<div class="partners-content d-flex flex-wrap justify-content-center">

		<?php
		ob_start();
		
			foreach ($partner as $key => $properties) {
				$photo = $properties['src'][0];
				if($photo == "") {
					$photo = get_bloginfo('template_directory') ."/img/lawyer/avatar.jpg";
				}
			?>

		        <div class="partners-item mx-4">
		          <img class="img-fluid" src="<?=$photo;?>" alt="?=$properties['title']?>">
		        </div>

			<?php } ?>

				      </div>
				</div>
			</div>
		</section>


		<?php }?>



	<?php
	$output = ob_get_contents();
	ob_end_clean();
	
	// Restore original Post Data
	wp_reset_postdata();	
	
	return $output;
}
?>