<?php
/*
Plugin Name: DP Works
Plugin URI: http://depewebdesign.com
Description: This plugin developed for showing Works listing on your website
Version: 1
Author: Denden Permana
Author URI: http://depewebdesign.com
Text Domain: dp-works
License: GPLv2
*/



// Our custom post type function
function create_posttype() {
 
    register_post_type( 'works',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Works' ),
                'singular_name' => __( 'Work' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'works'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Works', 'Post Type General Name', 'itechnosia' ),
        'singular_name'       => _x( 'Work', 'Post Type Singular Name', 'itechnosia' ),
        'menu_name'           => __( 'Works', 'itechnosia' ),
        'parent_item_colon'   => __( 'Parent Work', 'itechnosia' ),
        'all_items'           => __( 'All Works', 'itechnosia' ),
        'view_item'           => __( 'View Work', 'itechnosia' ),
        'add_new_item'        => __( 'Add New Work', 'itechnosia' ),
        'add_new'             => __( 'Add New', 'itechnosia' ),
        'edit_item'           => __( 'Edit Work', 'itechnosia' ),
        'update_item'         => __( 'Update Work', 'itechnosia' ),
        'search_items'        => __( 'Search Work', 'itechnosia' ),
        'not_found'           => __( 'Not Found', 'itechnosia' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'itechnosia' ),
    );


     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'works', 'itechnosia' ),
        'description'         => __( 'work news and reviews', 'itechnosia' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail'),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'topics', 'technologies' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-images-alt',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
    );
     
    // Registering your Custom Post Type
    register_post_type( 'works', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );



add_action( 'pre_get_posts', 'add_my_post_types_to_query' );
 
function add_my_post_types_to_query( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set( 'post_type', array( 'post', 'works' ) );
    return $query;
}


add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
  if( is_category() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'works'); // don't forget nav_menu_item to allow menus to work!
    $query->set('post_type',$post_type);
    return $query;
    }
}


/*  TOPICS TAXONOMIES */
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );

function create_topics_hierarchical_taxonomy() {
  $labels = array(
    'name' => _x( 'Topics', 'taxonomy general name' ),
    'singular_name' => _x( 'Topic', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Topics' ),
    'all_items' => __( 'All Topics' ),
    'parent_item' => __( 'Parent Topic' ),
    'parent_item_colon' => __( 'Parent Topic:' ),
    'edit_item' => __( 'Edit Topic' ), 
    'update_item' => __( 'Update Topic' ),
    'add_new_item' => __( 'Add New Topic' ),
    'new_item_name' => __( 'New Topic Name' ),
    'menu_name' => __( 'Topics' ),
  );    
  register_taxonomy('topics', array('works'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  )); 
}


/*  TECHNOLOGY TAXONOMIES */
add_action( 'init', 'create_technologies_hierarchical_taxonomy', 0 );

function create_technologies_hierarchical_taxonomy() {
  $labels = array(
    'name' => _x( 'Technologies', 'taxonomy general name' ),
    'singular_name' => _x( 'Technology', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Technologies' ),
    'all_items' => __( 'All Technologies' ),
    'parent_item' => __( 'Parent Technology' ),
    'parent_item_colon' => __( 'Parent Technology:' ),
    'edit_item' => __( 'Edit Technology' ), 
    'update_item' => __( 'Update Technology' ),
    'add_new_item' => __( 'Add New Technology' ),
    'new_item_name' => __( 'New Technology Name' ),
    'menu_name' => __( 'Technologies' ),
  );    
  register_taxonomy('technologies', array('works'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'technology' ),
  )); 
}
