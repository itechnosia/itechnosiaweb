<?php
/*
Plugin Name: Itechosia Settings
Plugin URI: http://depewebdesign.com.
Description: Setting all configuration of website
Version: 1.0.0.1
Author: Denden Permana
Author URI: http://depewebdesign.com
License: MIT
License URI: http://opensource.org/licenses/MIT
*/

class ItechnosiaSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Itechnosia Settings', 
            'manage_options', 
            'itechnosia-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'itechnosia_name' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Itechnosia Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'itechnosia_group' );   
                do_settings_sections( 'itechnosia-setting-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'itechnosia_group', // Option group
            'itechnosia_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        ); 

        add_settings_section(
            'setting_section_id', // ID
            'Itechnosia Website Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'itechnosia-setting-admin' // Page
        );     

        add_settings_field(
            'admin_gmap_latitude', // ID
            'Google Map Latitude', // Title 
            array( $this, 'admin_gmap_latitude_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );     

        add_settings_field(
            'admin_gmap_longitude', // ID
            'Google Map Longitude', // Title 
            array( $this, 'admin_gmap_longitude_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        ); 

        add_settings_field(
            'admin_telephone', // ID
            'Telephone', // Title 
            array( $this, 'admin_telephone_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );  

        add_settings_field(
            'admin_handphone', // ID
            'Handphone', // Title 
            array( $this, 'admin_handphone_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );    
		
		add_settings_field(
            'admin_email', // ID
            'Admin Email', // Title 
            array( $this, 'admin_email_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );   
		
		add_settings_field(
            'admin_facebook', // ID
            'Facebook Link', // Title 
            array( $this, 'admin_facebook_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );      
		
		add_settings_field(
            'admin_twitter', // ID
            'Twitter Link', // Title 
            array( $this, 'admin_twitter_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );          
		
		add_settings_field(
            'admin_youtube', // ID
            'Youtube Link', // Title 
            array( $this, 'admin_youtube_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );  

        add_settings_field(
            'admin_vimeo', // ID
            'Vimeo Link', // Title 
            array( $this, 'admin_vimeo_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );                
		
		add_settings_field(
            'admin_instagram', // ID
            'Instagram Link', // Title 
            array( $this, 'admin_instagram_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'admin_linkedin', // ID
            'Linkedin Link', // Title 
            array( $this, 'admin_linkedin_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );               
        
        add_settings_field(
            'admin_behance', // ID
            'Behance Link', // Title 
            array( $this, 'admin_behance_callback' ), // Callback
            'itechnosia-setting-admin', // Page
            'setting_section_id' // Section           
        );        
		
		         
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['admin_gmap_latitude'] ) )
            $new_input['admin_gmap_latitude'] = sanitize_text_field( $input['admin_gmap_latitude'] );

        if( isset( $input['admin_gmap_longitude'] ) )
            $new_input['admin_gmap_longitude'] = sanitize_text_field( $input['admin_gmap_longitude'] );

        if( isset( $input['admin_telephone'] ) )
            $new_input['admin_telephone'] = sanitize_text_field( $input['admin_telephone'] );            

        if( isset( $input['admin_handphone'] ) )
            $new_input['admin_handphone'] = sanitize_text_field( $input['admin_handphone'] );            

        if( isset( $input['admin_email'] ) )
            $new_input['admin_email'] = sanitize_text_field( $input['admin_email'] );
			
		if( isset( $input['admin_facebook'] ) )
            $new_input['admin_facebook'] = esc_url( $input['admin_facebook'] );
			
		if( isset( $input['admin_twitter'] ) )
            $new_input['admin_twitter'] = esc_url( $input['admin_twitter'] );
			
		if( isset( $input['admin_youtube'] ) )
            $new_input['admin_youtube'] = esc_url( $input['admin_youtube'] );

        if( isset( $input['admin_vimeo'] ) )
            $new_input['admin_vimeo'] = esc_url( $input['admin_vimeo'] );
			
		if( isset( $input['admin_instagram'] ) )
            $new_input['admin_instagram'] = esc_url( $input['admin_instagram'] );

        if( isset( $input['admin_linkedin'] ) )
            $new_input['admin_linkedin'] = esc_url( $input['admin_linkedin'] );

        if( isset( $input['admin_behance'] ) )
            $new_input['admin_behance'] = esc_url( $input['admin_behance'] );
			
			

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_gmap_latitude_callback()
    {
        printf(
            '<input type="text" id="admin_gmap_latitude" class="regular-text" name="itechnosia_name[admin_gmap_latitude]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_gmap_latitude'] ) ? esc_attr( $this->options['admin_gmap_latitude']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_gmap_longitude_callback()
    {
        printf(
            '<input type="text" id="admin_gmap_longitude" class="regular-text" name="itechnosia_name[admin_gmap_longitude]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_gmap_longitude'] ) ? esc_attr( $this->options['admin_gmap_longitude']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_telephone_callback()
    {
        printf(
            '<input type="text" id="admin_telephone" class="regular-text" name="itechnosia_name[admin_telephone]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_telephone'] ) ? esc_attr( $this->options['admin_telephone']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_handphone_callback()
    {
        printf(
            '<input type="text" id="admin_handphone" class="regular-text" name="itechnosia_name[admin_handphone]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_handphone'] ) ? esc_attr( $this->options['admin_handphone']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_email_callback()
    {
        printf(
            '<input type="text" id="admin_email" class="regular-text" name="itechnosia_name[admin_email]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_email'] ) ? esc_attr( $this->options['admin_email']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_facebook_callback()
    {
        printf(
            '<input type="text" id="admin_facebook" class="regular-text" name="itechnosia_name[admin_facebook]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_facebook'] ) ? esc_attr( $this->options['admin_facebook']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_twitter_callback()
    {
        printf(
            '<input type="text" id="admin_twitter" class="regular-text" name="itechnosia_name[admin_twitter]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_twitter'] ) ? esc_attr( $this->options['admin_twitter']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_youtube_callback()
    {
        printf(
            '<input type="text" id="admin_youtube" class="regular-text" name="itechnosia_name[admin_youtube]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_youtube'] ) ? esc_attr( $this->options['admin_youtube']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_vimeo_callback()
    {
        printf(
            '<input type="text" id="admin_vimeo" class="regular-text" name="itechnosia_name[admin_vimeo]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_vimeo'] ) ? esc_attr( $this->options['admin_vimeo']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_instagram_callback()
    {
        printf(
            '<input type="text" id="admin_instagram" class="regular-text" name="itechnosia_name[admin_instagram]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_instagram'] ) ? esc_attr( $this->options['admin_instagram']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_linkedin_callback()
    {
        printf(
            '<input type="text" id="admin_linkedin" class="regular-text" name="itechnosia_name[admin_linkedin]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_linkedin'] ) ? esc_attr( $this->options['admin_linkedin']) : ''
        );
    }
    

    /** 
     * Get the settings option array and print one of its values
     */
    public function admin_behance_callback()
    {
        printf(
            '<input type="text" id="admin_behance" class="regular-text" name="itechnosia_name[admin_behance]" value="%s" maxlength="255"  />',
            isset( $this->options['admin_behance'] ) ? esc_attr( $this->options['admin_behance']) : ''
        );
    }
    
	
	
	
	
}

if( is_admin() )
    $my_settings_page = new ItechnosiaSettings();