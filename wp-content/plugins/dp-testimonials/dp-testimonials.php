<?php
/**
 * Plugin Name: DP Testimonials
 * Plugin URI: http://itechnosia.com
 * Description: This plugin developed for showing testimonials listing on your website.
 * Version: 1.0.0
 * Author: Denden Permana
 * Author URI: http://itechnosia.com
 * Text Domain: dp-testimonials
 * License: GPL2
 */

add_action( 'init', 'dp_testimonials_tags', 0 );
function dp_testimonials_tags() {
  $labels = array(
		'name' => __('User Testimony', 'dp-testimonials'),
		'singular_name' => __('User Testimony', 'dp-testimonials'),
		'add_new' => __('Add New', 'dp-testimonials'),
		'add_new_item' => __('Add New Testimony', 'dp-testimonials'),
		'edit_item' => __('Edit Testimony', 'dp-testimonials'),
		'new_item' => __('New Testimony', 'dp-testimonials'),
		'view_item' => __('View Testimony', 'dp-testimonials'),
		'search_items' => __('Search Testimony', 'dp-testimonials'),
		'not_found' => __('No Testimony', 'dp-testimonials'),
		'not_found_in_trash' => __('No Testimonys found in Trash', 'dp-testimonials'),
		'parent_item_colon' => '',
		'menu_name' => __('Testimonials', 'dp-testimonials')
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 21,
		'menu_icon' => 'dashicons-format-quote',
		'supports' => array('title', 'editor', 'excerpt')
	); 
	register_post_type('testi', $args);
}

add_shortcode('dp-testimonials', 'testimony_shortcode');
function testimony_shortcode(){
	$args = array( 'post_type' => 'testi', 'posts_per_page' => '-1', 'orderby' => 'menu_order', 'order' => 'DESC');	
	$loop = new WP_Query( $args );	
	$testimony = array();
	
	while ( $loop->have_posts() ) :
		$loop->the_post();		
		$title = get_the_title();
		$content = get_the_content();
		$excerpt = get_the_excerpt();	
		$testimony[] = array('title' => $title, 'content' => $content, 'excerpt' => $excerpt);	
	endwhile;
	

	if(count($testimony) > 0) :
		?>

			<section class="page-section ps-testimonials bg-lightgreen-gradient"  style="background-image: url('<?php bloginfo('template_directory');?>/img/banner/free/chess.jpg');">
				<div class="container-fluid">
      
			      <h2 class="page-section-title text-center">Testimonials</h2>
			      <p class="page-section-subtitle text-center">What clients are saying</p>
					
					<div class="testimonials">
						<div class="testimonials-inner">

						<?php
							foreach ($testimony as $key => $testi) :
						?>
						<div class="testimonials-item text-center">
							<div class="testimonials-quote card border-0 shadow-sm">
								<div class="card-body">
									<p class="mb-0">
										<?php print $testi['content'];?>
									</p>	
								</div>
							</div>
							<div class="testimonials-author">
								<div class="testimonials-avatar mx-auto">
									<img class="img-fluid rounded-circle" src="img/testimonials/img1.jpg">
								</div>
								<h4 class="h6 mb-0"><?php print $testi['title'];?></h4>
								<p class="small"><?php print $testi['excerpt'];?></p>
							</div>
						</div>

						<?php
							endforeach;
						?>          


						</div>
					</div>
				</div>	
			</section>

		<?php
	endif;
	
	// Restore original Post Data
	wp_reset_postdata();	
	
	return $output;
}

?>