<?php
/**
 * Plugin Name: DP Homepage Gallery
 * Plugin URI: http://itechnosia.com
 * Description: This plugin developed for showing homegallery listing on your website.
 * Version: 1.0.0
 * Author: Denden Permana
 * Author URI: http://itechnosia.com
 * Text Domain: dp-homegallery
 * License: GPL2
 */

add_action( 'init', 'dp_homegallery_tags' );
function dp_homegallery_tags() {
  $labels = array(
		'name' => __('Gallery Image', 'dp-homegallery'),
		'singular_name' => __('Gallery Image', 'dp-homegallery'),
		'add_new' => __('Add New', 'dp-homegallery'),
		'add_new_item' => __('Add New Gallery Image', 'dp-homegallery'),
		'edit_item' => __('Edit Gallery Image', 'dp-homegallery'),
		'new_item' => __('New Gallery Image', 'dp-homegallery'),
		'view_item' => __('View Gallery Image', 'dp-homegallery'),
		'search_items' => __('Search Gallery Image', 'dp-homegallery'),
		'not_found' => __('No Gallery Image', 'dp-homegallery'),
		'not_found_in_trash' => __('No gallery Images found in Trash', 'dp-homegallery'),
		'parent_item_colon' => '',
		'menu_name' => __('Homepage Gallery', 'dp-homegallery')
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 21,
		'menu_icon' => 'dashicons-images-alt',
		'supports' => array('title', 'editor', 'thumbnail',)
	); 
	register_post_type('homegal', $args);
}

function homegal_get_featured_image($post_ID) {  
	$post_thumbnail_id = get_post_thumbnail_id($post_ID);  
	if ($post_thumbnail_id) {  
		$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');  
		return $post_thumbnail_img[0];  
	}  
}

function homegal_columns_head($defaults) {  
	$defaults['featured_image'] = __('Featured Image', 'dp-homegallery');  
	return $defaults;  
}  

function homegal_columns_content($column_name, $post_ID) {  
	if ($column_name == 'featured_image') {  
		$post_featured_image = homegal_get_featured_image($post_ID);  
		if ($post_featured_image) {  
			echo '<a href="'.get_edit_post_link($post_ID).'"><img width="150" src="' . $post_featured_image . '" /></a>';  
		}  
	}  
}

add_filter('manage_homegal_posts_columns', 'homegal_columns_head'); 
add_action('manage_homegal_posts_custom_column', 'homegal_columns_content', 10, 2);

 



// add shortcode
add_shortcode('dp-homegallery', 'homegal_shortcode');

function homegal_shortcode(){
	$args = array( 'post_type' => 'homegal', 'posts_per_page' => '-1', 'orderby' => 'menu_order', 'order' => 'ESC');	
	$loop = new WP_Query( $args );	
	$images = array();
	
	while ( $loop->have_posts() ) {
		$loop->the_post();

		
		
		if ( '' != get_the_post_thumbnail() ) {
			$post_ID 			= get_the_ID();
			$title 				= get_the_title();
			$content 			= get_the_content();
			$image 				= get_the_post_thumbnail( $post_ID, 'full' );
			$post_secondary_img = MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'secondary-image');			
			$post_thumbnail_id 	= get_post_thumbnail_id($post_ID);  
			$src 				= wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview'); 
			$images[] 			= array(
									'title' 		=> $title, 
									'content' 		=> $content, 
									'image' 		=> $image, 
									'secondary' 	=> $post_secondary_img,
									'src' 			=> $src);			
		}
	}

	if(count($images) > 0){
		ob_start();
		?>

		 <section class="page-section ps-bannercarousel banner-carousel is-has-wave">
			<div class="banner-carousel-inner">  

          		<?php
					foreach ($images as $key => $image) { ?>				
						<div class="banner-carousel-item">  
		          			<div class="banner-carousel-image" style="background-image: url('<?=$image['src'][0]?>');">
		          				<div class="container-fluid">
		          					<div class="banner-carousel-text text-center">
		          						<div class="banner-carousel-content">
		          							<div class="banner-carousel-description">
		          								<h1 class="banner-carousel-title"><?=$image['title'];?></h1>
		          								<p class="banner-carousel-subtitle text-typing"><?=$image['content'];?></p>
		          							</div>
		          						</div>
		          					</div>
		          				</div>
		          			</div>
		          		</div>
						<?php
					}
				?>
		</div>
			<figure class="bg-wave bg-pos-bottom-wave">
				<img class="js-svg-injector" src="<?php bloginfo('template_directory');?>/img/bg/wave-bottom.svg" alt="wave background">
			</figure>
		</section>



		<?php	

		$output = ob_get_contents();
		ob_end_clean();
	}
	
	// Restore original Post Data
	wp_reset_postdata();	
	
	return $output;
}
?>