<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', getenv("databasename"));

/** MySQL database username */
define('DB_USER', getenv("databaseusername"));

/** MySQL database password */
define('DB_PASSWORD', getenv("databasepassword"));

/** MySQL hostname */
define('DB_HOST', getenv("MYSQL_SERVICE_HOST"));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ih>U]1IGReeUtDaQ~W%kGvIfW>4suFD5~#ff-t<W{[9X%gZ>MO9Fq@{Wc$sJPK6t');
define('SECURE_AUTH_KEY',  'CBI7V3tM*_;-=a&Nx*Jo}hY%W_qAy9z@RR?^U>VS<!X 3:J}2Y^j.` >sH8f-; b');
define('LOGGED_IN_KEY',    ' l@1+Vk-T=a?~ekr>,{d03A7|0/=_R{xBv+Pw.K<O044=*QZfKH;r8G9NV9H%uH*');
define('NONCE_KEY',        'l^<xVS0Gu@S2F,yA]j]A:p?yEY]<w[DC:x$$AzS-lK`Iq*WDpcr{EC,J BwnREs4');
define('AUTH_SALT',        '7iFJ+;z+LT&YJSF<mntS%T3Qc ^<h~rQcZhSbm5O7;2<=ItEPQ kN SKKkd;4*<c');
define('SECURE_AUTH_SALT', '3Q!X9[bH8%D!L,:3^[a2*NEL})>jq.^U9dC,@:=|[m[5Hd>QBnJa)jZHK!|2^eL%');
define('LOGGED_IN_SALT',   've5Y;k>`,K+3b{0/5Tj.Cp5<0tq(EvzE+-u!Sp  Lz7lrw]5C<$u5DD0^Wq>R=aT');
define('NONCE_SALT',       'QKw&Kl6s6B[^6FC=yFEy$-=,HtgfGhDct4^E}FQ$+H@C?f]w~UuCRr-X4 9gL>@%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
